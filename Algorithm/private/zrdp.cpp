#include "zrdp.h"
#include <sys/time.h>

ZRDP::ZRDP(int tK, int m_packetsize): ZCode(tK,2,1,m_packetsize)
{
    if(K>46)
    {
        printf("K should be smaller than 47\n");
        exit(-1);
    }
    realK = tK;
    int i;
    for(i = 0;i<15;i++)
        if(tK + 1 <= prime[i])
        {
            K = prime[i] - 1;
            R = K;
            break;
        }
    printf("ZRDP K=%d,R=%d,realK=%d\n",K,R,realK);
    //    struct timeval t0,t1;
    //    gettimeofday(&t0,NULL);
    //    gettimeofday(&t1,NULL);
    //    init_time = diff_us(t0,t1);

    //    for(i = 0;i<en_scheduleLen;i++)
    //    {
    //        printf("%d,%d,%d,%d,%d\n", en_schedule[i][0],en_schedule[i][1],en_schedule[i][2],en_schedule[i][3],en_schedule[i][4]);
    //        //        if(en_schedule[i][4] == 0)
    //        //            n_cpy ++;
    //        //        else
    //        //            n_xor ++;
    //        //        i++;
    //    }
    if(packetsize == 0)
        packetsize = find_packetsize(realK*R+2);

    ptrs = (char**)aligned_alloc(64,(realK+2)*sizeof(char*));
    assert(ptrs != NULL);
    Pidx = realK;
    Qidx = realK+1;
    gen_encode_schedule();
    en_schedule->printSchLen("RDP encode");
    blocksize = packetsize * realK * R;
//    printf("Pidx = %d, Qidx = %d\n", Pidx, Qidx);
}

ZRDP::~ZRDP()
{
    free(ptrs);
    //    free(decodeIDs);
}

static int modulo (int a, int b) { return a >= 0 ? a % b : ( b - abs ( a%b ) ) % b; }

// 0 cpy to parity
// 1 xor to parity
void ZRDP::gen_encode_schedule()
{
    en_schedule = new ZSchedule(packetsize);
    // P
    for(int l = 0;l<R;l++)
    {
        for(int t = 0;t<realK;t++)
        {
            en_schedule->addSch(t,l,Pidx,l);
        }
    }

    // Copy P[i+1] to Q[i]
    for(int t=0;t<R-1;t++)
    {
        en_schedule->addSch(Pidx, t+1, Qidx, t);
    }

    // Q
    for(int l = 0;l<R;l++)
    {
        int x = 0;
        int y = l;
        // XOR diag
        for(int t=0;t<R;t++)
        {
            //            printf("this %d,%d, realK = %d, R = %d\n",x,y,realK,R);
            if(x < realK && y < R)
                en_schedule->addSch(x,y,Qidx,l);
            nextURDiag(x,y);
            //            printf("next %d,%d\n",x,y);
        }
    }
//    en_schedule->printSch();
}

void ZRDP::encode_single_chunk(char* data, int len, char**& parities)
{
    assert(data != NULL);
    assert(parities != NULL);
    assert(len ==  packetsize * realK * R);
    for(int i = 0;i<realK;i++)
    {
        ptrs[i] = data + i*packetsize*R;
//        printf("enc_buf[%d] = %p\n",i,ptrs[i]);
    }
    ptrs[Pidx] = parities[0];
    ptrs[Qidx] = parities[1];
//    printf("%p,%p,%p,%p\n", parities[0], ptrs[Pidx], parities[1], ptrs[Qidx]);
//    printPTRs();
    en_schedule->run(ptrs);
//    printPTRs();
}

void ZRDP::set_erasure(vector<int> arr)
{
    unsigned long long pattern = 0LL;
    int i;
    if(arr.size() != 2)
    {
        printf("lost disk other than 2 is not support\n");
        exit(-1);
    }

    for(i = 0;i<arr.size();i++)
    {
        pattern |= 1LL << arr[i];
    }


//    if(de_schedules_map.find(pattern) != de_schedules_map.end())
//    {
//        printf("found decode schedule for pattern %llu\n",pattern);
//        de_schedule = de_schedules_map[pattern];
//    }
//    else
//    {
        printf("new decode schedule for pattern %llu\n",pattern);
        gen_decode_schedule(pattern);
        assert(de_schedule != NULL);
//        de_schedules_map[pattern] = de_schedule;
//    }

}

void ZRDP::nextURDiag(int &x, int &y)
{
    x++;
    if(x >= K) x = 0;
    y--;
    if(y<0) y = R;
}

// return i > 0 is diag start from (0,i) contains no missing element in matrix;
// return -2 is solved
int ZRDP::findFullDiag()
{
    int acnt = 0;
    int min = R>realK?realK:R;
    for(int i = 0;i<R;i++)
    {
//        printf("checking diag %d\n",i);
        int x = 0;
        int y = i;
        bool ret = true;
        for(int j = 0;j<min;j++)
        {
            if(matrix[x][y] == 0 && y != R)
            {
//                printf("found missing ele %d,%d\n",x,y);
                acnt++;
                ret = false;
                break;
            }
            nextURDiag(x,y);
        }
        if(ret && i != R-1 && solvedP.find(i+1) == solvedP.end()) return i;
    }
    if(acnt == 0) return -2;
    else
        return -1;
}


// return i > 0 is diag start from (0,i) contains only one missing element in matrix;
// return -1 if S diag
// return -2 is solved
int ZRDP::find1lessDiag()
{
    int acnt = 0;
//    int min = R>realK?realK:R;
    for(int i = 0;i<R;i++)
    {
//        printf("checking diag %d\n",i);
        int cnt = 0;
        int x = 0;
        int y = i;
        for(int j = 0;j<realK;j++)
        {
//            printf("checking diag %d: %d,%d\n",i,x,y);
            if(matrix[x][y] == 0 && y != R)
            {
                acnt++;
                cnt ++;
                if(cnt > 1) break;
//                printf("found 0 @ %d,%d, cnt = %d\n", x,y,cnt);
                missingX = x;
                missingY = y;
            }
            nextURDiag(x,y);
        }
        if(cnt == 1) return i;
    }
    if(acnt == 0) return -2;
    // check S
    else
    {
        printf("find1lessDiag return -1\n");
        return -1;
    }
}

void ZRDP::printMatrix()
{
    for(int i = 0;i<R;i++)
    {
        for(int j = 0;j<Pidx;j++)
            printf("%d ", matrix[j][i]);
        printf("\n");
    }
}

void ZRDP::gen_decode_schedule(unsigned long long pattern)
{
    bool lostP = pattern & (1 << Pidx);
    bool lostQ = pattern & (1 << Qidx);
    int lostDat0=-1, lostDat1=-1;
    for(int i = 0;i<realK;i++)
    {
        if(pattern & (1<<i))
            if(lostDat0 < 0)
                lostDat0 = i;
            else
                lostDat1 = i;
    }

    printf("lostP=%d, lostQ=%d, lostD0=%d, lostD1=%d\n", lostP, lostQ, lostDat0, lostDat1);

    de_schedule = new ZSchedule(packetsize);
    if(lostP && !lostQ) // not repair P for now
    {
        printf("gen schedule lost %d and P\n", lostDat0);

        // recover d[lostDat0,R-1-lostDat0] using main diag and Q[R-1]
        de_schedule->addSch(Qidx,R-1,lostDat0,R-1-lostDat0);
        int x = 0;
        int y = R-1;
        for(int i = 0;i<K;i++)
        {
            //            printf("x,y =%d.%d\n",x,y);
            if(x < realK && y < R && x != lostDat0)
            {
                de_schedule->addSch(x,y,lostDat0,R-1-lostDat0);
                //                printf("d[%d,%d] -> %d,%d\n",x,y,lostDat0,R-1-lostDat0);
            }
            nextURDiag(x,y);
        }
        // recover P[R-1-lostDat0]
        for(int i = 0;i<realK;i++)
            de_schedule->addSch(i,R-1-lostDat0,Pidx,R-1-lostDat0);
        solvedP.clear();
        solvedP.insert(R-1-lostDat0);

        matrix.resize(Pidx);
        for(int i = 0;i<Pidx;i++)
        {
            if(i == lostDat0)
                matrix[i].assign(R, 0);
            else
                matrix[i].assign(R, 1);
        }
        matrix[lostDat0][R-1-lostDat0] = 1;

        int lastSolvedP = R-1-lostDat0;
        int knownQ;
        int cnt = 0;

        while(lastSolvedP != 0)
        {
            knownQ = lastSolvedP - 1;
            int x = 0;
            int y = knownQ;
            int solvableD = knownQ-lostDat0;
            if(solvableD < 0)
                solvableD = modulo(solvableD,R) + 1;
//            printf("dat[%d,%d] solvable\n",lostDat0, solvableD);
            de_schedule->addSch(Pidx,lastSolvedP,lostDat0,solvableD);
            de_schedule->addSch(Qidx,knownQ,lostDat0,solvableD);


            for(int j = 0;j<K;j++)
            {
                if(x < realK && y < R && x != lostDat0)
                {
                        de_schedule->addSch(x,y,lostDat0,solvableD);
                }
                nextURDiag(x,y);
            }
            matrix[lostDat0][solvableD] = 1;

            // solve P[solvableD]
            for(int j = 0;j<realK;j++)
            {
                de_schedule->addSch(j,solvableD,Pidx,solvableD);
            }

            solvedP.insert(solvableD);
            lastSolvedP = solvableD;
        }

        while (1)
        {
//            printMatrix();
            int solvableP;
            int knownQ = findFullDiag();
            if(knownQ == -2)
                break;
            else if(knownQ == -1)
            {
                printf("No full diag\n");
                cnt++;
            }
            else
            {
//                printf("Diag Q[%d] is solvable\n", knownQ);
                if(knownQ != R - 1)
                {
                    solvableP = knownQ + 1;
//                    printf("solving P[%d]\n",solvableP);
                    de_schedule->addSch(Qidx,knownQ,Pidx,solvableP);
                    // solve P[solvableP];
                    int x = 0;
                    int y = knownQ;

                    for(int j = 0;j<K;j++)
                    {
                        if(x < realK && y < R)
                        {
                            de_schedule->addSch(x,y,Pidx,solvableP);
//                            printf("d[%d,%d] -> P[%d]\n",x,y,solvableP);
                        }
                        nextURDiag(x,y);
                    }
                    solvedP.insert(solvableP);

                    // solve d[lostDat0, solvableP]
                    de_schedule->addSch(Pidx,solvableP,lostDat0,solvableP);
                    for(int j = 0;j<realK;j++)
                    {
                        if(j != lostDat0)
                        {
                            de_schedule->addSch(j,solvableP,lostDat0,solvableP);
//                            printf("d[%d,%d] -> P[%d]\n",x,y,solvableP);
                        }
                    }
                    matrix[lostDat0][solvableP] = 1;
                }
                else
                {
                    printf("Unknown condition knownQ = R-1\n");
                    continue;
                }
                cnt ++;
            }
//            if(cnt >= 1) break;
        }
    }
    else if(lostQ && !lostP) // not repair Q for now
    {
        printf("gen schedule lost %d and Q\n", lostDat0);
        for(int l = 0;l<R;l++)
        {
            // XOR P
            de_schedule->addSch(Pidx,l,lostDat0,l);
            for(int t = 0;t<Pidx;t++)
            {
                if(t == lostDat0) continue;
                de_schedule->addSch(t,l,lostDat0,l);
            }
        }
    }
    else if(lostP && lostQ)
    {
        de_schedule = en_schedule;
        return;
    }
    else // lost 2 data
    {
        printf("lost 2 data %d, %d\n", lostDat0, lostDat1);
        matrix.resize(Pidx);
        for(int i = 0;i<Pidx;i++)
        {
            if(i == lostDat0 || i == lostDat1)
                matrix[i].assign(R, 0);
            else
                matrix[i].assign(R, 1);
        }
//        printMatrix();
        int diag,x,y;
        while((diag = find1lessDiag()) != -2)
        {
//            printf("diag from (0,%d) contains only 1 missing entry @ %d,%d\n",diag, missingX, missingY);

//            printMatrix();

            // XOR Q
            de_schedule->addSch(Qidx, diag, missingX, missingY);
//            printf("Q[%d] -> %d,%d\n", diag, missingX, missingY);

            // XOR P if needed
            if(diag != R-1)
            {
                de_schedule->addSch(Pidx,diag+1,missingX,missingY);
//                printf("P[%d] -> %d,%d\n", diag+1, missingX, missingY);
            }

            x = 0;
            y = diag;

            for(int j = 0;j<R;j++)
            {
//                printf("x,y = %d,%d\n",x,y);
                if(x < realK && y < R && x != missingX)
                {
                    de_schedule->addSch(x,y,missingX,missingY);
//                    printf(" diag %d,%d -> %d,%d\n", x,y, missingX, missingY);
                }
                nextURDiag(x,y);
            }

            matrix[missingX][missingY] = 1;
            int missX;
            if(missingX == lostDat0) missX = lostDat1;
            else missX = lostDat0;

            // Write P
            de_schedule->addSch(Pidx,missingY,missX, missingY);
            // Write Other Data
            for(int j = 0;j<realK;j++)
            {
                if(j == missX) continue;
                de_schedule->addSch(j,missingY,missX, missingY);
            }
            matrix[missX][missingY] = 1;

//            printf("after iteration\n");
//            printMatrix();
        }
    }
}

void ZRDP::printPTRs()
{
    printf("== print PTR s\n");
    for(int i = 0;i<R;i++)
    {
        for(int j = 0;j<Qidx+1;j++)
        {
            if(j == Pidx)
                printf("|\t");
            printf("%d\t", ptrs[j][i]);
        }
        printf("\n");
    }
}

void ZRDP::decode_single_chunk(char **&data, char **&parities)
{
    for(int i = 0;i<realK;i++)
        ptrs[i] = data[i];
    ptrs[Pidx] = parities[0];
    ptrs[Qidx] = parities[1];

    de_schedule->run(ptrs);
//    printPTRs();
}
