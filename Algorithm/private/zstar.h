﻿#ifndef ZSTAR_H
#define ZSTAR_H
#include <stdlib.h>
#include <string.h>
#include <map>
#include <set>
#include <cassert>
#include <vector>
#include "../zcode.h"
#include "zschedule.h"
#include "../../utils.h"
using namespace std;

class ZStar : public ZCode
{
public:
    ZStar(int tK, int packetsize);
    ~ZStar();
    void encode_single_chunk(char* data, int len, char**& parities);
    void set_erasure(vector<int> arr);
    void decode_single_chunk(char** &data, char** &parities);
    int K;
    ZSchedule* de_schedule;
    void printPTRs(char* p);
private:
    const int prime[15] = {2, 3, 5, 7, 11, 13, 17, 19, 23, 29, 31, 37, 41, 43, 47};
    int realK;
    int R;
    ZSchedule* en_schedule;
    char** ptrs;
    char* S1;
    char* S2;
    char* S12;
    map<unsigned long long, ZSchedule*> de_schedules_map;
    void gen_encode_schedule();
    void gen_decode_schedule(unsigned long long pattern);
    void gen_decode_DPQ(int d);
    void gen_decode_DPZ(int d);
    void gen_decode_DQZ(int d);
    void gen_decode_D2P(int d0, int d1);
    void gen_decode_D2Q(int d0, int d1);
    void gen_decode_D2Z(int d0, int d1, bool calc_S);
    void gen_decode_D3(int d0, int d1, int d2);
    void nextURDiag(int& x, int& y);
    void nextDRDiag(int& x, int& y);
    int find1lessDiag();
    int find1lessAntidiag();
    vector<vector<int> >matrix;
    void printMatrix();
    int missingX;
    int missingY;
    int Pidx;
    int Qidx;
    int Zidx;
    int S1idx;
    int S2idx;
    int S12idx; // used only in D2P, share space with S1

//    vector<vector<int> > s_hat;
//    vector<vector<int> > a_telda;
    char** s_telda;
    int s_teldaPidx;
    int s_teldaQidx;
    int s_teldaRidx;
    bool isCopyParity;

    vector<pair<int, int> > crosses;
    vector<int> minusP;
    int r;
    int s;
    int t;
    int u;
    int v;
    set<int> solvedInS;
    int complement;

    void xor_insert(set<int> &s, int v);
    void xor_merge_set(set<int> &s1, set<int> &s2);
    int findCross();
    int get_ld(int u,int v,int p);
    set<int> missingInCross(int x0, int y0, int x1, int y1);
};

#endif // ZSTAR_H
