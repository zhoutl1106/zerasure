﻿#ifndef ZRAID6_H
#define ZRAID6_H
#include <stdlib.h>
#include <string.h>
#include <map>
#include <set>
extern "C"{
#include "../../Jerasure-1.2A/jerasure.h"
#include "../../Jerasure-1.2A/cauchy.h"
#include "../../Jerasure-1.2A/reed_sol.h"
}
#include <cassert>
#include <vector>
#include "../zcode.h"
#include "zschedule.h"
using namespace std;

class ZRaid6 : public ZCode
{
public:
    ZRaid6(int tK, int packetsize);
    ~ZRaid6();
    void encode_single_chunk(char* data, int len, char**& parities);
    void set_erasure(vector<int> arr);
    void decode_single_chunk(char** &data, char** &parities);
    int K;
private:
    char** datp;
    int* erasures;
    int* enc_matrix;
};

#endif // ZRAID6_H
