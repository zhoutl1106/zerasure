#include "zevenodd.h"
#include <sys/time.h>

ZEvenOdd::ZEvenOdd(int tK, int m_packetsize): ZCode(tK,2,1,m_packetsize)
{
    if(K>47)
    {
        printf("K should be smaller than 48\n");
        exit(-1);
    }
    realK = tK;
    int i;
    for(i = 0;i<15;i++)
        if(tK <= prime[i])
        {
            K = prime[i];
            R = K-1;
            break;
        }
    printf("ZEvenOdd K=%d,R=%d,realK=%d\n",K,R,realK);
    struct timeval t0,t1;
    gettimeofday(&t0,NULL);
    gettimeofday(&t1,NULL);
    init_time = diff_us(t0,t1);

    if(packetsize == 0)
        packetsize = find_packetsize(realK*R+2);

    posix_memalign((void**)(&S),64,packetsize);
//    printf("S addr = %p\n", S);
    ptrs = (char**)aligned_alloc(64,(realK+3)*sizeof(char*));
//    for(int i= 0;i<realK+3;i++)
//        printf("ptrs[%d] = %p\n", i, ptrs[i]);
    assert(ptrs != NULL);
    assert(S != NULL);
    Pidx = realK;
    Qidx = realK+1;
    Sidx = realK+2;
    ptrs[Sidx] = S;
//    printf("ptrs[Sidx] addr = %p\n", ptrs[Sidx]);
    gen_encode_schedule();
    en_schedule->printSchLen("Evenodd Encoding : ");
    blocksize = packetsize*realK*R;
    //    free(matrix);
}

ZEvenOdd::~ZEvenOdd()
{
    free(ptrs);
    //    free(decodeIDs);
    free(S);
}

int modulo (int a, int b) { return a >= 0 ? a % b : ( b - abs ( a%b ) ) % b; }

// 0 cpy to parity
// 1 xor to parity
void ZEvenOdd::gen_encode_schedule()
{
    en_schedule = new ZSchedule(packetsize);
    // P
    for(int l = 0;l<R;l++)
    {
        for(int t = 0;t<realK;t++)
        {
            en_schedule->addSch(t,l,Pidx,l);
        }
    }

    // S
    for(int t=1;t<=K-1;t++)
    {
        if(t >= Pidx) break;
        en_schedule->addSch(t,K-1-t,Sidx,0);
    }

    // Q
    for(int l = 0;l<R;l++)
    {
        // XOR S
        en_schedule->addSch(Sidx,0,Qidx,l);

        // XOR diag
        for(int t=0;t<Pidx;t++)
        {
            if(modulo(l-t,K) >= R) continue;
            en_schedule->addSch(t,modulo(l-t,K),Qidx,l);
        }
    }
    en_schedule->printSchLen("en_schedule");
}

void ZEvenOdd::encode_single_chunk(char* data, int len, char**& parities)
{
    assert(data != NULL);
    assert(parities != NULL);
    assert(len ==  packetsize * realK * R);
    for(int i = 0;i<realK;i++)
    {
        ptrs[i] = data + i*packetsize*R;
        //        printf("enc_buf[%d] = %p\n",i,encode_buf[i]);
    }
    ptrs[Pidx] = parities[0];
    ptrs[Qidx] = parities[1];
    memset(parities[0],0,R*packetsize);
    memset(parities[1],0,R*packetsize);
    memset(S,0,packetsize);
//    printf("%p,%p,%p,%p\n", parities[0], ptrs[Pidx], parities[1], ptrs[Qidx]);
//    printPTRs();
    en_schedule->run(ptrs);
//    printPTRs();
//    printf("after encode, S=%d,%p/%d,%p, %d\n", S[0],S,ptrs[Sidx][0],ptrs[Sidx],Sidx);
}

void ZEvenOdd::set_erasure(vector<int> arr)
{
    unsigned long long pattern = 0LL;
    int i;
    if(arr.size() != 2)
    {
        printf("lost disk other than 2 is not support\n");
        exit(-1);
    }

    for(i = 0;i<arr.size();i++)
    {
        pattern |= 1LL << arr[i];
    }

    if(de_schedules_map.find(pattern) != de_schedules_map.end())
    {
        printf("found decode schedule for pattern %llu\n",pattern);
        de_schedule = de_schedules_map[pattern];
    }
    else
    {
        printf("new decode schedule for pattern %llu\n",pattern);
        gen_decode_schedule(pattern);
        assert(de_schedule != NULL);
        de_schedules_map[pattern] = de_schedule;
    }
}

void ZEvenOdd::nextURDiag(int &x, int &y)
{
    x++;
    if(x >= K) x = 0;
    y--;
    if(y<0) y = R;
}

// return i > 0 is diag start from (0,i) contains only one missing element in matrix;
// return -1 if S diag
// return -2 is solved
int ZEvenOdd::find1lessDiag()
{
    int acnt = 0;
    for(int i = 0;i<R;i++)
    {
//        printf("checking diag %d\n",i);
        int cnt = 0;
        int x = 0;
        int y = i;
        for(int j = 0;j<realK;j++)
        {
//            printf("checking diag %d: %d,%d\n",i,x,y);
            if(matrix[x][y] == 0 && y != R)
            {
                acnt++;
                cnt ++;
                if(cnt > 1) break;
//                printf("found 0 @ %d,%d, cnt = %d\n", x,y,cnt);
                missingX = x;
                missingY = y;
            }
            nextURDiag(x,y);
        }
        if(cnt == 1) return i;
    }
    if(acnt == 0) return -2;
    // check S
    else
        return -1;
}

void ZEvenOdd::printMatrix()
{
    for(int i = 0;i<R;i++)
    {
        for(int j = 0;j<realK;j++)
            printf("%d ", matrix[j][i]);
        printf("\n");
    }
}

void ZEvenOdd::gen_decode_schedule(unsigned long long pattern)
{
    bool lostP = pattern & (1 << Pidx);
    bool lostQ = pattern & (1 << Qidx);
    int lostDat0=-1, lostDat1=-1;
    for(int i = 0;i<realK;i++)
    {
        if(pattern & (1<<i))
        {
            if(lostDat0 < 0)
                lostDat0 = i;
            else
                lostDat1 = i;
        }
    }

//    printf("lostP=%d, lostQ=%d, lostD0=%d, lostD1=%d\n", lostP, lostQ, lostDat0, lostDat1);

    de_schedule = new ZSchedule(packetsize);
    if(lostP && !lostQ) // not repair P for now
    {
        printf("gen schedule lost %d and P\n", lostDat0);
        // S
        if(lostDat0 != 0)
        {
            de_schedule->addSch(Qidx,modulo(lostDat0-1,R),Sidx,0);
        }
        for(int i = 0;i<R;i++)
        {
            int pos = modulo(lostDat0-1-i,K);
            if(pos >= realK || pos == lostDat0) continue;
            de_schedule->addSch(pos,i,Sidx,0);
        }

        // for each ele in missing row
        for(int i = 0;i<R;i++)
        {
            // XOR S
            de_schedule->addSch(Sidx,0,lostDat0,i);
            //            printf("S, %d,0 -> %d,%d\n",Sidx,Qidx,i);

            // XOR Q
            if(i + lostDat0 != R)
            {
                int dst = i+lostDat0;
                if(dst > R) dst -= K;
                de_schedule->addSch(Qidx,dst,lostDat0,i);
                //                printf("Q, %d,%d -> %d,%d\n",lostDat0,dst,Qidx,i);
            }


            int x = lostDat0 + 1;
            if(x >= K) x = 0;
            int y = i - 1;
            if(y<0) y = R;
            for(int l=0;l<R;l++)
            {
//                printf("x,y = %d,%d\n",x,y);
                if((x != lostDat0 && x != Pidx && y < R && x < Pidx))
                {
                    de_schedule->addSch(x,y,lostDat0,i);
//                    printf("diag, %d,%d -> %d,%d\n",x,y,lostDat0,i);
                }
                nextURDiag(x,y);
            }
        }
    }
    else if(lostQ && !lostP) // not repair Q for now
    {
        printf("gen schedule lost %d and Q\n", lostDat0);
        for(int l = 0;l<R;l++)
        {
            // XOR P
            de_schedule->addSch(Pidx,l,lostDat0,l);
            for(int t = 0;t<Pidx;t++)
            {
                if(t == lostDat0) continue;
                de_schedule->addSch(t,l,lostDat0,l);
            }
        }
    }
    else if(lostP && lostQ)
    {
        de_schedule = en_schedule;
        return;
    }
    else // lost 2 data
    {
        printf("gen schedule lost 2 data %d, %d\n", lostDat0, lostDat1);
        // S = all C[i,j]
        for(int i = 0;i<R;i++)
        {
            de_schedule->addSch(Pidx,i,Sidx,0);
            de_schedule->addSch(Qidx,i,Sidx,0);
        }
        matrix.resize(Pidx);
        for(int i = 0;i<Pidx;i++)
        {
            if(i == lostDat0 || i == lostDat1)
                matrix[i].assign(R, 0);
            else
                matrix[i].assign(R, 1);
        }
//        printMatrix();
        int diag,x,y;
        while((diag = find1lessDiag()) != -2)
        {
//            printf("diag from (0,%d) contains only 1 missing entry @ %d,%d\n",diag, missingX, missingY);

//            printMatrix();
            // Write S
            de_schedule->addSch(Sidx,0,missingX,missingY);

            x = missingX;
            y = missingY;
            if(x == K - 1)
            {
                int yy = y - 1;
                if(yy < 0) yy = R-1;
//                printf(" add Q[%d]->%d,%d\n",yy, missingX, missingY);
                de_schedule->addSch(Qidx, yy, missingX, missingY);
            }
            nextURDiag(x,y);
            for(int j = 0;j<R;j++)
            {
//                printf("x,y = %d,%d\n",x,y);
                if(x < Pidx && y < R)
                {
                    de_schedule->addSch(x,y,missingX,missingY);
//                    printf(" diag %d,%d -> %d,%d\n", x,y, missingX, missingY);
                }
                if(x == K-1) // time to right Q
                {
                    int yy = y-1;
                    if(yy < 0) yy = R-1;
//                    printf(" add Q[%d]->%d,%d\n",yy, missingX,missingY);
                    de_schedule->addSch(Qidx, yy, missingX, missingY);
                }
                nextURDiag(x,y);
            }

            matrix[missingX][missingY] = 1;
            int missX;
            if(missingX == lostDat0) missX = lostDat1;
            else missX = lostDat0;

            // Write P
            de_schedule->addSch(Pidx,missingY,missX, missingY);
            // Write Other Data
            for(int j = 0;j<realK;j++)
            {
                if(j == missX) continue;
                de_schedule->addSch(j,missingY,missX, missingY);
            }
            matrix[missX][missingY] = 1;


//            getchar();
        }
    }
    de_schedule->printSchLen("de_schedule");
//    de_schedule->printSch();
}

void ZEvenOdd::printPTRs()
{
    printf("== print PTR s\n");
    for(int i = 0;i<R;i++)
    {
        for(int j = 0;j<Sidx;j++)
        {
            if(j == Pidx)
                printf("| %d\t", ptrs[j][i]);
            else
                printf("%d\t", ptrs[j][i]);
        }
        printf("\n");
    }
    printf("S = %d\n", ptrs[Sidx][0]);
}

int checkzero(char*p, int len)
{
    for(int i = 0;i<len;i++)
        if(p[i] != 0)
            return 0;
    return 1;
}

void ZEvenOdd::decode_single_chunk(char **&data, char **&parities)
{
    for(int i = 0;i<realK;i++)
        ptrs[i] = data[i];
    ptrs[Pidx] = parities[0];
    ptrs[Qidx] = parities[1];

    memset(S,0,packetsize);

//    printf("S=0?%d\n", checkzero(S,packetsize));
    de_schedule->run(ptrs);
//    printPTRs();
//    printf("S=%d\n",S[0]);
}
