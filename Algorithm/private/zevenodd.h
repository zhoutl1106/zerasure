#ifndef ZEVENODD_H
#define ZEVENODD_H
#include <stdlib.h>
#include <string.h>
#include <map>
extern "C"{
#include "../../Jerasure-1.2A/jerasure.h"
#include "../../Jerasure-1.2A/cauchy.h"
}
#include <cassert>
#include <vector>
#include "../zcode.h"
#include "zschedule.h"
#include "../../utils.h"

using namespace std;

class ZEvenOdd : public ZCode
{
public:
    ZEvenOdd(int tK, int packetsize);
    ~ZEvenOdd();
    void encode_single_chunk(char* data, int len, char**& parities);
    void set_erasure(vector<int> arr);
    void decode_single_chunk(char** &data, char** &parities);
    int K;
private:
    void printPTRs();
    const int prime[15] = {2, 3, 5, 7, 11, 13, 17, 19, 23, 29, 31, 37, 41, 43, 47};
    int realK;
    int R;
    ZSchedule* en_schedule;
    ZSchedule* de_schedule;
    char** ptrs;
    char* S;
    map<unsigned long long, ZSchedule*> de_schedules_map;
    void gen_encode_schedule();
    void gen_decode_schedule(unsigned long long pattern);
    void nextURDiag(int& x, int& y);
    int find1lessDiag();
    vector<vector<int> >matrix;
    void printMatrix();
    int missingX;
    int missingY;
    int Pidx;
    int Qidx;
    int Sidx;
};

#endif // ZEVENODD_H
