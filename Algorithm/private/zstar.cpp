#include "zstar.h"
#include <sys/time.h>

void printSet(set<int> &s, char* p)
{
    printf("%s: ", p);
    for(set<int>::iterator it=s.begin(); it != s.end(); it++)
        printf(" %d", *it);
    printf("\n");
}
void printArray(vector<int> a, char* p)
{
    printf("%s: ", p);
    for(int i = 0;i<a.size();i++)
        printf("%d ", a[i]);
    printf("\n");
}

ZStar::ZStar(int tK, int m_packetsize): ZCode(tK,3,1,m_packetsize)
{
    if(K>47)
    {
        printf("K should be smaller than 48\n");
        exit(-1);
    }
    realK = tK;
    int i;
    for(i = 0;i<15;i++)
        if(tK <= prime[i])
        {
            K = prime[i];
            R = K-1;
            break;
        }
    printf("ZStar K=%d,R=%d,realK=%d\n",K,R,realK);
    struct timeval t0,t1;
    gettimeofday(&t0,NULL);
    gettimeofday(&t1,NULL);
    init_time = diff_us(t0,t1);
    if(packetsize == 0)
        packetsize = find_packetsize(realK*R+3);

    posix_memalign((void**)(&S1),64,packetsize);
    posix_memalign((void**)(&S2),64,packetsize);
    posix_memalign((void**)(&S12),64,packetsize);
    ptrs = (char**)aligned_alloc(64,(realK+9)*sizeof(char*));
    s_telda = malloc2d(3, K*packetsize); // s_telda[1,2][R] store XOR of missings in S1,S2
    assert(ptrs != NULL);
    assert(S1 != NULL);
    assert(S2 != NULL);
    assert(S12 != NULL);
    Pidx = realK;
    Qidx = realK+1;
    Zidx = realK+2;
    s_teldaPidx = realK+3;
    s_teldaQidx = realK+4;
    s_teldaRidx = realK+5;
    S1idx = realK+6;
    S2idx = realK+7;
    S12idx = realK+8;

    ptrs[S1idx] = S1;
    ptrs[S2idx] = S2;
    ptrs[S12idx] = S12;
    ptrs[s_teldaPidx] = s_telda[0];
    ptrs[s_teldaQidx] = s_telda[1];
    ptrs[s_teldaRidx] = s_telda[2];

    gen_encode_schedule();
    en_schedule->parent = this;
    en_schedule->printSchLen("Star encode");
    blocksize = packetsize * realK * R;
}

ZStar::~ZStar()
{
    free(ptrs);
    free(S1);
}

//static int modulo (int a, int b) { return a >= 0 ? a % b : ( b - abs ( a%b ) ) % b; }
static int modulo (int a, int b) { return (a+b)%b; }

// 0 cpy to parity
// 1 xor to parity
void ZStar::gen_encode_schedule()
{
    en_schedule = new ZSchedule(packetsize);
    // P
    for(int l = 0;l<R;l++)
    {
        for(int t = 0;t<realK;t++)
        {
            en_schedule->addSch(t,l,Pidx,l);
        }
    }

    // S1
    for(int t=1;t<=K-1;t++)
    {
        if(t >= Pidx) break;
        en_schedule->addSch(t,K-1-t,S1idx,0);
    }

    // S2
    for(int t=0;t<R;t++)
    {
        if(t+1 >= Pidx) break;
        en_schedule->addSch(t+1,t,S2idx,0);
    }

    // Q
    for(int l = 0;l<R;l++)
    {
        // XOR S
        en_schedule->addSch(S1idx,0,Qidx,l);

        // XOR diag
        for(int t=0;t<Pidx;t++)
        {
            if(modulo(l-t,K) >= R) continue;
            en_schedule->addSch(t,modulo(l-t,K),Qidx,l);
        }
    }

    // Z
    for(int l = 0;l<R;l++)
    {
        // XOR S
        en_schedule->addSch(S2idx,0,Zidx,l);

        // XOR anti-diag
        for(int t=0;t<Pidx;t++)
        {
            if(modulo(l+t,K) >= R) continue;
            //            printf("Z diag %d, %d\n",t, modulo(l+t,K));
            en_schedule->addSch(t,modulo(l+t,K),Zidx,l);
        }
    }
    //    en_schedule->printSch();
}

void ZStar::encode_single_chunk(char* data, int len, char**& parities)
{
    memset(S1,0,packetsize);
    memset(S2,0,packetsize);
    memset(S12,0,packetsize);
    assert(data != NULL);
    assert(parities != NULL);
    assert(len ==  packetsize * realK * R);
    for(int i = 0;i<realK;i++)
    {
        ptrs[i] = data + i*packetsize*R;
    }
    ptrs[Pidx] = parities[0];
    ptrs[Qidx] = parities[1];
    ptrs[Zidx] = parities[2];
//    for(int i = 0;i<S2idx;i++)
//        printf("ptrs[%d] = %p\n",i,ptrs[i]);

//    printPTRs("before encode");
    //    en_schedule->printSchLen("en_schedule");
    en_schedule->run(ptrs);
//    printPTRs("after encode");
}

void ZStar::set_erasure(vector<int> arr)
{
    unsigned long long pattern = 0LL;
    int i;
    if(arr.size() != 3)
    {
        printf("lost disk other than 3 is not support\n");
        exit(-1);
    }

    for(i = 0;i<arr.size();i++)
    {
        pattern |= 1LL << arr[i];
    }

    bool lostP = pattern & (1 << Pidx);
    bool lostQ = pattern & (1 << Qidx);
    bool lostZ = pattern & (1 << Zidx);

    if((!lostP && !lostQ && !lostZ) || 1)
    {
        isCopyParity = true;
    }
    else
        isCopyParity = false;

    crosses.clear();
    minusP.clear();
    solvedInS.clear();

    if(de_schedules_map.find(pattern) != de_schedules_map.end())
    {
        printf("found decode schedule for pattern %llu\n",pattern);
        de_schedule = de_schedules_map[pattern];
    }
    else
    {
        printf("new decode schedule for pattern %llu\n",pattern);
        gen_decode_schedule(pattern);
        assert(de_schedule != NULL);
        de_schedules_map[pattern] = de_schedule;
    }
}

void ZStar::nextURDiag(int &x, int &y)
{
    x++;
    if(x >= K) x = 0;
    y--;
    if(y<0) y = R;
}

void ZStar::nextDRDiag(int &x, int &y)
{
    x++;
    if(x >= K) x = 0;
    y++;
    if(y==K)
    {
        y = 0;
    }
    //    printf("Next DR: %d,%d\n",x,y);
}

// return i > 0 is diag start from (0,i) contains only one missing element in matrix;
// return -1 if S diag
// return -2 is solved
int ZStar::find1lessDiag()
{
    int acnt = 0;
    for(int i = 0;i<R;i++)
    {
        //        printf("checking diag %d\n",i);
        int cnt = 0;
        int x = 0;
        int y = i;
        for(int j = 0;j<K;j++)
        {
            //            printf("checking diag %d: %d,%d\n",i,x,y);
            if(y < R && x < realK && matrix[x][y] == 0)
            {
                acnt++;
                cnt ++;
                if(cnt > 1) break;
                //                printf("found 0 @ %d,%d, cnt = %d\n", x,y,cnt);
                missingX = x;
                missingY = y;
            }
            nextURDiag(x,y);
        }
        if(cnt == 1) return i;
    }
    if(acnt == 0) return -2;
    // check S
    else
        return -1;
}

// return i > 0 is diag start from (0,i) contains only one missing element in matrix;
// return -1 if S diag
// return -2 is solved
int ZStar::find1lessAntidiag()
{
    int acnt = 0;
    for(int i = 0;i<R;i++)
    {
        //        printf("checking diag %d\n",i);
        int cnt = 0;
        int x = 0;
        int y = i;
        for(int j = 0;j<K;j++)
        {
            //            printf("checking %d, diag %d: %d,%d\n",j,i,x,y);
            if(y < R && x < realK && matrix[x][y] == 0)
            {
                acnt++;
                cnt ++;
                //                printf("found 0 @ %d,%d, cnt = %d\n", x,y,cnt);
                if(cnt > 1) break;
                missingX = x;
                missingY = y;
            }
            nextDRDiag(x,y);
        }
        if(cnt == 1) return i;
    }
    if(acnt == 0) return -2;
    // check S
    else
        return -1;
}

void ZStar::printMatrix()
{
    for(int i = 0;i<R;i++)
    {
        for(int j = 0;j<Pidx;j++)
            printf("%d ", matrix[j][i]);
        printf("\n");
    }
}

void ZStar::gen_decode_schedule(unsigned long long pattern)
{
    bool lostP = pattern & (1 << Pidx);
    bool lostQ = pattern & (1 << Qidx);
    bool lostZ = pattern & (1 << Zidx);
    int lostDat[3] = {-1,-1,-1};
    int lostIdx = 0;
    for(int i = 0;i<realK;i++)
    {
        if(pattern & (1<<i))
        {
            lostDat[lostIdx] = i;
            lostIdx++;
        }
    }

    printf("lostP=%d, lostQ=%d, lostZ = %d, lostD0=%d, lostD1=%d, lostD2=%d\n", lostP, lostQ, lostZ, lostDat[0], lostDat[1], lostDat[2]);

    ////    de_schedule = (int**)malloc2d(K*K*R,5*sizeof(int));
    de_schedule = new ZSchedule(packetsize);
    de_schedule->parent = this;
    int pqz = (lostP<<2) + (lostQ<<1) + (lostZ);
    switch(pqz)
    {
    case 0:
        printf("lost 3D %d, %d, %d\n",lostDat[0], lostDat[1], lostDat[2]);
        gen_decode_D3(lostDat[0],lostDat[1],lostDat[2]);
        break;
    case 1:
        printf("lost 2D %d,%d and Z\n", lostDat[0], lostDat[1]);
        gen_decode_D2Z(lostDat[0], lostDat[1],true);
        break;
    case 2:
        printf("lost 2D %d,%d and Q\n", lostDat[0], lostDat[1]);
        gen_decode_D2Q(lostDat[0], lostDat[1]);
        break;
    case 3:
        printf("lost 1D %d and QZ\n", lostDat[0]);
        gen_decode_DQZ(lostDat[0]);
        break;
    case 4:
        printf("lost 2D %d,%d and P\n", lostDat[0], lostDat[1]);
        gen_decode_D2P(lostDat[0], lostDat[1]);
        break;
    case 5:
        printf("lost 1D %d and PZ\n", lostDat[0]);
        gen_decode_DPZ(lostDat[0]);
        break;
    case 6:
        printf("lost 1D %d and PQ\n", lostDat[0]);
        gen_decode_DPQ(lostDat[0]);
        break;
    case 7:
        printf("lost 0D and PQZ\n");
        de_schedule = en_schedule;
        break;
    }
}


void ZStar::gen_decode_DPQ(int d)
{
    printf("gen schedule lost %d and P, Q\n", d);
    // S2
    if(d == 0)
    {
        for(int i = 0;i<R;i++)
            if(i+1 < realK && i < R)
                de_schedule->addSch(i+1,i,S2idx,0);
    }
    else
    {
        de_schedule->addSch(Zidx,R-d,S2idx,0);
        int x = d-1, y = R-1;
        for(int i = 0;i<K;i++)
        {
            if(x < realK && x != d && y < R)
            {
//                printf("recover S2, [%d,%d]", x, y);
                de_schedule->addSch(x,y,S2idx,0);
            }
            nextDRDiag(x,y);
        }
    }

    // for each ele in missing row
    for(int i = 0;i<R;i++)
    {
        // XOR S2
        de_schedule->addSch(S2idx,0,d,i);
        //            printf("S, %d,0 -> %d,%d\n",Sidx,Qidx,i);

        // XOR Z
        if(i + K - d != R)
        {
            int dst = i-d;
            if(dst < 0) dst += K;
            de_schedule->addSch(Zidx,dst,d,i);
//            printf("Z, %d,%d -> %d,%d\n",Zidx,dst,d,i);
        }


        int x = d;
        int y = i;
        nextDRDiag(x,y);
        for(int l=0;l<R;l++)
        {
//            printf("x,y = %d,%d\n",x,y);
            if((x != d && x != Pidx && y < R && x < Pidx))
            {
                de_schedule->addSch(x,y,d,i);
//                printf("diag, %d,%d -> %d,%d\n",x,y,d,i);
            }
            nextDRDiag(x,y);
        }
    }
}

void ZStar::gen_decode_DPZ(int d)
{
    printf("gen schedule lost %d and P, Z\n", d);
    // S1
    if(d != 0)
    {
        de_schedule->addSch(Qidx,modulo(d-1,R),S1idx,0);
    }
    for(int i = 0;i<R;i++)
    {
        int pos = modulo(d-1-i,K);
        if(pos >= realK || pos == d) continue;
        de_schedule->addSch(pos,i,S1idx,0);
    }

    // for each ele in missing row
    for(int i = 0;i<R;i++)
    {
        // XOR S1
        de_schedule->addSch(S1idx,0,d,i);
        //            printf("S, %d,0 -> %d,%d\n",Sidx,Qidx,i);

        // XOR Q
        if(i + d != R)
        {
            int dst = i+d;
            if(dst > R) dst -= K;
            de_schedule->addSch(Qidx,dst,d,i);
            //                printf("Q, %d,%d -> %d,%d\n",lostDat0,dst,Qidx,i);
        }


        int x = d + 1;
        if(x >= K) x = 0;
        int y = i - 1;
        if(y<0) y = R;
        for(int l=0;l<R;l++)
        {
//            printf("x,y = %d,%d\n",x,y);
            if((x != d && x != Pidx && y < R && x < Pidx))
            {
                de_schedule->addSch(x,y,d,i);
//                printf("diag, %d,%d -> %d,%d\n",x,y,d,i);
            }
            nextURDiag(x,y);
        }
    }
}

void ZStar::gen_decode_DQZ(int d)
{
    printf("gen schedule lost %d and Q,Z\n", d);
    for(int l = 0;l<R;l++)
    {
        // XOR P
        de_schedule->addSch(Pidx,l,d,l);
        for(int t = 0;t<Pidx;t++)
        {
            if(t == d) continue;
            de_schedule->addSch(t,l,d,l);
        }
    }
    //    de_schedule->printSch();
}

void ZStar::gen_decode_D2P(int d0, int d1)
{
    r = d0;
    s = d1;
    t = realK;

    printf("==== START D2P , missing %d,%d====\n",r,s);

    // restore S12
    for(int i = 0;i<R;i++)
    {
        de_schedule->addSch(Qidx,i,S12idx,0);
        de_schedule->addSch(Zidx,i,S12idx,0);
    }

    // inverse adjuster, now all s_telda[1,2] = XOR missings + S1,2
    for(int i = 0;i<R;i++)
        for(int j = 0;j<K;j++)
        {
            int x = j;
            int yq = modulo(K+i-j,K);
            int yz = modulo(i+j,K);
            if(j != r && j != s)
            {
                if(x < realK && yq < R)
                {
                    de_schedule->addSch(x,yq,s_teldaQidx,i);
//                    printf("0 s~Q: [%d,%d] -> [%d,%d]\n", x, yq, s_teldaQidx,i);
                }
                if(x < realK && yz < R)
                {
                    de_schedule->addSch(x,yz,s_teldaRidx,i);
//                    printf("1 s~R: [%d,%d] -> [%d,%d]\n", x, yz, s_teldaRidx,i);
                }
            }
        }

    // s_telda[1,2][R] = missing of diags + S1,2
    int i = R;
    for(int j = 0;j<realK;j++)
    {
        int x = j;
        int yq = modulo(K+i-j,K);
        int yz = modulo(i+j,K);
        //        printf("%d,%d,%d\n",x,yq,yz);
        if(j != r && j != s && j != t && yq < R && yz < R)
        {
            de_schedule->addSch(x,yq,s_teldaQidx,R);
            de_schedule->addSch(x,yz,s_teldaRidx,R);
//            printf("2: %d,%d->Q[%d]\n",x,yq,R);
//            printf("3: %d,%d->R[%d]\n",x,yz,R);
        }
    }

    solvedInS.clear();
    i = 0;
    int cnt = 0;
    while(1) // recover s_deltaP
    {
        int solvable = -1;
        set<int> missings = missingInCross(r,i,s,i);
//        printSet(missings, "missings");
        set<int>::iterator it = missings.begin();
        if(missings.size() == 2 && solvedInS.find(*missings.begin()%R) == solvedInS.end())
        {
            int v0 = *it++;
            int v1 = *it;
            solvable = v0%R;
//            printf("direct solve P[%d], have %d,%d\n", solvable, v0,v1);

            // copy S1+S2 to P[solvable];
            de_schedule->addSch(S12idx,0,s_teldaPidx,solvable);

            int q1,r1;
            q1 = modulo(i + s, K);
            r1 = modulo(i - r, K);

            //            printf("add Q[%d] to P[%d]\n", q1,solvable);
            //            printf("add R[%d] to P[%d]\n", r1,solvable);

            de_schedule->addSch(s_teldaQidx,q1,s_teldaPidx,solvable);
            de_schedule->addSch(s_teldaRidx,r1,s_teldaPidx,solvable);

        }
        else if(missings.size() == 4)
        {
            int v0 = *it++;
            int v1 = *it++;
            int v2 = *it++;
            int v3 = *it;
            int p0 = v0 % R;
            int p1 = v1 % R;
            solvable = -1;
//                        printf("FOUR %d,%d,%d,%d, | %d,%d\n",v0,v1,v2,v3,p0,p1);
            if(solvedInS.find(p0) == solvedInS.end() && solvedInS.find(p1) != solvedInS.end())
            {
                complement = p1;
                solvable = p0;
            }
            else if(solvedInS.find(p1) == solvedInS.end() && solvedInS.find(p0) != solvedInS.end())
            {
                complement = p0;
                solvable = p1;
            }
            else
            {
                i = (i+1) % R;
                continue;
            }
//            printf("complement solve P[%d] using %d, have %d,%d,%d,%d\n", solvable, complement,v0,v1,v2,v3);

            // copy S1+S2 to P[solvable];
            de_schedule->addSch(S12idx,0,s_teldaPidx,solvable);

            // copy complement to P[solvable]
            de_schedule->addSch(s_teldaPidx,complement,s_teldaPidx,solvable);

            int q1,r1;
            q1 = modulo(i + s, K);
            r1 = modulo(i - r, K);

//            printf("add Q[%d] to P[%d]\n", q1,solvable);
//            printf("add R[%d] to P[%d]\n", r1,solvable);

            de_schedule->addSch(s_teldaQidx,q1,s_teldaPidx,solvable);
            de_schedule->addSch(s_teldaRidx,r1,s_teldaPidx,solvable);
        }
        else
        {
//            printSet(missings, "unknown missings");
//            getchar();
        }
        if(solvable != -1)
        {
//            printf("inserting %d\n", solvable);
            solvedInS.insert(solvable);
//            printSet(solvedInS, "Solved in S");
            if(solvedInS.size() == R)
            {
//                printf("all P solved\n");
                break;
            }
        }
        i = (i+1) % R;
        cnt ++;
//        if(cnt == 9) break;
    }

    // recover P from s_deltaP
    for(int i = 0;i<R;i++)
    {
        de_schedule->addSch(s_teldaPidx,i,Pidx,i);
        for(int j = 0;j<realK;j++)
        {
            if(j != r && j != s)
            {
                de_schedule->addSch(j,i,Pidx,i);
//                printf("4: %d,%d->%d,%d\n",j,i,Pidx,i);
            }
        }
    }
    gen_decode_D2Z(d0,d1,true);
}

void ZStar::gen_decode_D2Q(int d0, int d1)
{
    printf("lost 2 data %d, %d and Q\n", d0, d1);
    // S1 = all P[i,j] and Q[i,j]
    for(int i = 0;i<R;i++)
    {
        de_schedule->addSch(Pidx,i,S2idx,0);
        de_schedule->addSch(Zidx,i,S2idx,0);
    }
    matrix.resize(Pidx);
    for(int i = 0;i<Pidx;i++)
    {
        if(i == d0 || i == d1)
            matrix[i].assign(R, 0);
        else
            matrix[i].assign(R, 1);
    }
    int diag,x,y;
    while((diag = find1lessAntidiag()) != -2)
    {
//        printf("antidiag from (0,%d) contains only 1 missing entry @ %d,%d\n",diag, missingX, missingY);

//        printMatrix();
        // Write S
        de_schedule->addSch(S2idx,0,missingX,missingY);
//        printf("S2 -> %d,%d\n", missingX, missingY);

        x = missingX;
        y = missingY;
        //        if(x == K - 1)
        //        {
        int yy = modulo(y-x,K);
        //            if(yy < 0) yy = R-1;
//        printf(" add Z[%d]->%d,%d\n",yy, missingX, missingY);
        de_schedule->addSch(Zidx, yy, missingX, missingY);
        //        }
        nextDRDiag(x,y);
        for(int j = 0;j<R;j++)
        {
//            printf("x,y = %d,%d\n",x,y);
            if(x < Pidx && y < R && x != missingX && y != missingY)
            {
                de_schedule->addSch(x,y,missingX,missingY);
//                printf(" diag %d,%d -> %d,%d\n", x,y, missingX, missingY);
            }
            //            if(x == K-1) // time to write Z
            //            {
            //                int yy = y+1;
            //                if(yy >= R) yy = 0;
            //                printf(" add Z[%d]->%d,%d\n",yy, missingX,missingY);
            //                de_schedule->addSch(Zidx, yy, missingX, missingY);
            //            }
            nextDRDiag(x,y);
        }

        matrix[missingX][missingY] = 1;
        int missX;
        if(missingX == d0) missX = d1;
        else missX = d0;

        // Write P
        de_schedule->addSch(Pidx,missingY,missX, missingY);
        // Write Other Data
        for(int j = 0;j<realK;j++)
        {
            if(j == missX) continue;
            de_schedule->addSch(j,missingY,missX, missingY);
        }
        matrix[missX][missingY] = 1;
//        printf("after iteration\n");
//        printMatrix();
//        printf("--------\n");
    }
}

void ZStar::gen_decode_D2Z(int d0, int d1, bool calc_S)
{
    printf("lost 2 data %d, %d and Z\n", d0, d1);
    if(calc_S)
    {
        // S1 = all P[i,j] and Q[i,j]
        for(int i = 0;i<R;i++)
        {
            de_schedule->addSch(Pidx,i,S1idx,0);
            de_schedule->addSch(Qidx,i,S1idx,0);
        }
    }
    matrix.resize(Pidx);
    for(int i = 0;i<Pidx;i++)
    {
        if(i == d0 || i == d1)
            matrix[i].assign(R, 0);
        else
            matrix[i].assign(R, 1);
    }
//        printMatrix();
    int diag,x,y;
    int cnt = 0;
    while((diag = find1lessDiag()) != -2)
    {
//                printf("-- cnt = %d, diag from (0,%d) contains only 1 missing entry @ %d,%d\n",cnt,diag, missingX, missingY);

//                printMatrix();
        // Write S
        de_schedule->addSch(S1idx,0,missingX,missingY);

        x = missingX;
        y = missingY;
        if(x == K - 1)
        {
            int yy = y - 1;
            if(yy < 0) yy = R-1;
//                        printf(" add Q[%d]->%d,%d\n",yy, missingX, missingY);
            de_schedule->addSch(Qidx, yy, missingX, missingY);
        }
        nextURDiag(x,y);
        for(int j = 0;j<R;j++)
        {
//                        printf("x,y = %d,%d\n",x,y);
            if(x < Pidx && y < R)
            {
                de_schedule->addSch(x,y,missingX,missingY);
//                                printf(" diag %d,%d -> %d,%d\n", x,y, missingX, missingY);
            }
            if(x == K-1) // time to write Q
            {
                int yy = y-1;
                if(yy < 0) yy = R-1;
//                                printf(" add Q[%d]->%d,%d\n",yy, missingX,missingY);
                de_schedule->addSch(Qidx, yy, missingX, missingY);
            }
            nextURDiag(x,y);
        }

        matrix[missingX][missingY] = 1;
        int missX;
        if(missingX == d0) missX = d1;
        else missX = d0;

        // Write P
        de_schedule->addSch(Pidx,missingY,missX, missingY);
//        printf(" add P[%d]->%d,%d\n",missingY, missX,missingY);
        // Write Other Data
        for(int j = 0;j<realK;j++)
        {
            if(j == missX) continue;
            de_schedule->addSch(j,missingY,missX, missingY);
//            printf("5: %d,%d->%d,%d\n",j,missingY,missX, missingY);
        }
        matrix[missX][missingY] = 1;
        cnt ++;
//        if(cnt == 6) break;
    }
}

void ZStar::gen_decode_D3(int d0, int d1, int d2)
{
    r = d0;
    s = d1;
    t = d2;
    u = s-r;
    v = t-s;
        printf("==== START D3 , missing %d,%d,%d====\n",r,s,t);

    // restore S1 and S2
    for(int i = 0;i<R;i++)
    {
        de_schedule->addSch(Pidx,i,S1idx,0);
        de_schedule->addSch(Qidx,i,S1idx,0);
        de_schedule->addSch(Pidx,i,S2idx,0);
        de_schedule->addSch(Zidx,i,S2idx,0);
    }

    // reversion adjuster, xor S1 to Q and S2 to Z
    for(int i = 0;i<R;i++)
    {
        de_schedule->addSch(S1idx,0,s_teldaQidx,i);
        de_schedule->addSch(S2idx,0,s_teldaRidx,i);
        //        a[i][p+1] = a[i][p+1]^s1;
        //        a[i][p+2] = a[i][p+2]^s2;
    }

    // xor known elements, s_telda -> xor of MISSING
    for(int i = 0;i<R;i++)
        for(int j = 0;j<K;j++)
        {
            int x = j;
            int yp = i;
            int yq = modulo(K+i-j,K);
            int yz = modulo(i+j,K);
            //            printf("%d,%d,%d,%d\n",x,yp,yq,yz);
            if(j != r && j != s && j != t)
            {
                if(x < realK && yp < R)
                {
                    de_schedule->addSch(x,yp,s_teldaPidx,i);
//                    printf("6: %d,%d->P[%d]\n",x,yp,i);
                }
                if(x < realK && yq < R)
                {
//                    printf("7: %d,%d->Q[%d]\n",x,yq,i);
                    de_schedule->addSch(x,yq,s_teldaQidx,i);
                }
                if(x < realK && yz < R)
                {
//                    printf("8: %d,%d->Q[%d]\n",x,yz,i);
                    de_schedule->addSch(x,yz,s_teldaRidx,i);
                }
            }
        }

    // calc missings in S1,S2, to s_telda[1,2][R]
    // s_telda_Q
    // XOR S1
    de_schedule->addSch(S1idx,0,s_teldaQidx,R);
    // XOR diag
    for(int tt=1;tt<realK;tt++)
    {
        if((tt==r) || (tt==s) || (tt==t)) continue;
//        printf("s_telda_Q [%d,%d]\n", tt, R-tt);
        de_schedule->addSch(tt,R-tt,s_teldaQidx,R);
    }

    // s_telda_R
    // XOR S2
    de_schedule->addSch(S2idx,0,s_teldaRidx,R);
    // XOR diag
    for(int tt=1;tt<realK;tt++)
    {
        if((tt==r) || (tt==s) || (tt==t)) continue;
//        printf("s_telda_Q [%d,%d]\n", tt, R-tt);
        de_schedule->addSch(tt,tt-1,s_teldaRidx,R);
    }


    int cnt = 0;
    while(1)
    {
        int solvable = findCross();
        if(solvable == -1)
        {
//            printf("Cannot find, exiting...\n");
            break;
        }
//                printf("find solvable %d using cross of", solvable);
//                for(int i = 0;i<crosses.size();i++)
//                    printf(" (%d,%d)", crosses[i].first, crosses[i].second);
//                printf("\n");

        if(complement == -1)
        {
//                        printf("direct solve\n");
        }
        else
        {
            //            //                    val = a[complement][s];
//                        printf("need complement %d\n", complement);
            de_schedule->addSch(s,complement, s, solvable);
            //            //            isBreak = true;
        }
//                        printf("resolve using s_telda\n");

        for(int i = 0;i<minusP.size();i++)
        {
//                        printf("add P[%d] to %d,%d\n", minusP[i],s,solvable);
            de_schedule->addSch(s_teldaPidx,minusP[i], s, solvable);
        }

        for(int i = 0;i<crosses.size();i++)
        {
            int q1,r1;
            q1 = modulo(crosses[i].second + s, K);
            r1 = modulo(crosses[i].first - s, K);

//                        printf("add R[%d] to %d,%d\n", r1,s,solvable);

//                printf("add ~Q[%d] to %d,%d\n", q1,s,solvable);
                de_schedule->addSch(s_teldaQidx,q1, s, solvable);
//                printf("add ~R[%d] to %d,%d\n", r1,s,solvable);
                de_schedule->addSch(s_teldaRidx,r1, s, solvable);
        }

        solvedInS.insert(solvable);
//                printSet(solvedInS, "Solved in S");
        if(solvedInS.size() == R) break;
//                printf("%d\n", cnt++);
//                                if(cnt == 1) break;
        //        getchar();
        //        break;
    }
    gen_decode_D2Z(d0,d2,false);
}

void ZStar::printPTRs(char* p)
{
    printf("== print PTR s %s\n",p);
    for(int i = 0;i<R;i++)
    {
//        for(int j = 0;j<s_teldaRidx+1;j++)
//        {
//            if(j == Pidx || j == s_teldaPidx)
//                printf("|\t");
//            if(i<R || (i == R && j >= s_teldaPidx))
//                printf("%d\t", ptrs[j][i]);
//            else
//                printf("\t");
//        }
        for(int j = 0;j<K;j++)
            if(j < realK)
                printf("%4d", ptrs[j][i]);
            else
                printf("   *");
        printf("    |");
        for(int j = realK;j<realK+3;j++)
            printf("%4d", ptrs[j][i]);
        printf("    ||");
        for(int j = realK+3;j<realK+6;j++)
            printf("%4d", ptrs[j][i]);
        printf("\n");
    }
    printf("%*s||    ", ((K+3)*4+9),"");
    for(int j = realK+4;j<realK+6;j++)
        printf("%4d", ptrs[j][R]);
    printf("\n");

    printf("S1 = %d, S2 = %d, S12 = %d\n", ptrs[S1idx][0],ptrs[S2idx][0], ptrs[S12idx][0]);
}

void ZStar::decode_single_chunk(char **&data, char **&parities)
{
    for(int i = 0;i<realK;i++)
        ptrs[i] = data[i];
    ptrs[Pidx] = parities[0];
    ptrs[Qidx] = parities[1];
    ptrs[Zidx] = parities[2];
    ptrs[S1idx] = S1;
    ptrs[S2idx] = S2;
    ptrs[S12idx] = S12;
    ptrs[s_teldaPidx] = s_telda[0];
    ptrs[s_teldaQidx] = s_telda[1];
    ptrs[s_teldaRidx] = s_telda[2];

//    for(int i = 0;i<S2idx;i++)
//        printf("ptrs[%d] = %p\n",i,ptrs[i]);

    memset(S1,0,packetsize);
    memset(S2,0,packetsize);
    memset(S12,0,packetsize);
    memset(s_telda[0],0,packetsize*K);
    memset(s_telda[1],0,packetsize*K);
    memset(s_telda[2],0,packetsize*K);
//    printPTRs("before decode");
    if(isCopyParity)
    {
//        printf("copy parities to s_telda\n");
        memcpy(s_telda[0], parities[0], packetsize*R);
        memcpy(s_telda[1], parities[1], packetsize*R);
        memcpy(s_telda[2], parities[2], packetsize*R);
    }

    //    de_schedule->printSchLen("de_schedule");
    de_schedule->run(ptrs);
//        printPTRs("after decode");
//        for(int i = 0;i<S2idx;i++)
//            printf("ptrs[%d] = %p\n",i,ptrs[i]);
}

void ZStar::xor_insert(set<int> &s, int v)
{
    if(s.find(v) != s.end())
    {
        //        printf("set remove %d\n",v);
        s.erase(v);
    }
    else
    {
        //        printf("set insert %d\n", v);
        s.insert(v);
    }
}

void ZStar::xor_merge_set(set<int> &s1, set<int> &s2)
{
    for(set<int>::iterator it = s2.begin(); it != s2.end(); it++)
    {
        xor_insert(s1, *it);
    }
}

set<int> ZStar::missingInCross(int x0, int y0, int x1, int y1)
{
    set<int> missings;
//            printf("\n## check cross (%d,%d)x(%d,%d), rst = %d,%d,%d\n", x0,y0,x1,y1,r,s,t);
    if(y0 < R)
        missings.insert(x0*R+y0);

    // insert all down slop missing to set
    for(int j = 0;j<R;j++)
    {
        nextDRDiag(x0,y0);
//        printf("   check antidiag %d,%d\n",x0,y0);
        if((x0 == r || x0 == s || x0 == t) && y0 < R && x0 < realK)
        {
//                                                printf("   check antidiag %d,%d, insert %d\n",x0,y0, x0*R+y0);
            missings.insert(x0*R+y0);
        }
    }

//            printf("\n-- check up slop %d,%d\n", x1,y1);
    if(y1 < R)
        xor_insert(missings, x1*R+y1);

    // insert all up slop missing to set
    for(int jj = 0;jj<R;jj++)
    {
        nextURDiag(x1,y1);
//        printf("   check diag %d,%d\n",x1,y1);
        if((x1 == r || x1 == s || x1 == t) && y1 < R && x1 < realK)
        {
//                                                printf("   check diag %d,%d, insert %d\n",x1,y1, x1*R+y1);
            xor_insert(missings, x1*R+y1);
        }
    }
//    printf(" == missings : ");
//    for(set<int>::iterator it = missings.begin(); it != missings.end(); it++)
//        printf("%d ", *it);
//    printf(" ==\n");
    return missings;
}

int ZStar::get_ld(int u, int v, int p)
{
    //    printf("finding ld for u = %d, v = %d, p = %d\n", u, v, p);
    for(int i = 0;i<K;i++)
        if(modulo(u+i*v,p)==0)
            return i;
}

int ZStar::findCross()
{
    int ld = get_ld(u,v,K);
//        printf("there are %d crosses in ring\n", ld);
    set<int> missings;
    set<int> missings1;
    //    int xi,yi,xj,yj;

    // any cross of (s,i)x(s,j)
    for(int i = 0;i<K;i++)
    {
//                printf("\n## check down slop %d,%d\n", s,i);
        for(int j = 0;j<K;j++)
        {
            missings = missingInCross(s,i,s,j);
//                        printSet(missings, "\nfirst cross");
            for(int jj = 1;jj<ld;jj++)
            {
                missings1 = missingInCross(s, modulo(i+jj*v,K), s, modulo(j+jj*v,K));
                xor_merge_set(missings, missings1);
//                printSet(missings, "after one merge");
            }
//                        printf("after %d cross (%d,%d)x(%d,%d): ",ld, s,i,s,j);
//                        printSet(missings, "");

            minusP.clear();
            for(int jj = 0;jj<R;jj++)
            {
                                //                printf("for crossing check row %d\n", jj);
                if(missings.find(r*R+jj) != missings.end() && missings.find(s*R+jj) != missings.end() && missings.find(t*R+jj) != missings.end())
                {
//                                        printf("   remove row %d of %d, %d, %d\n", jj, r*R+jj, s*R+jj, t*R+jj);
                    minusP.push_back(jj);

                    missings.erase(r*R+jj);
                    missings.erase(s*R+jj);
                    missings.erase(t*R+jj);
                }
            }
//                        printSet(missings, "after elimination");
            if(missings.size() == 1 && solvedInS.find(modulo(*(missings.begin()),R)) == solvedInS.end() && *(missings.begin())/R == s)
            {
//                                printf("direct solve [%d,%d]\n", s,*(missings.begin()));
                complement = -1;
                crosses.clear();
                for(int jj = 0;jj<ld;jj++)
                    crosses.push_back(make_pair(modulo(i+jj*v,K), modulo(j+jj*v,K)));
                return modulo(*(missings.begin()), R);
            }
            else if(missings.size() == 2)
            {
                crosses.clear();
                for(int jj = 0;jj<ld;jj++)
                    crosses.push_back(make_pair(modulo(i+jj*v,K), modulo(j+jj*v,K)));
                set<int>::iterator it=missings.begin();
                int v0 = *it++;
                int v1 = *it;
//                                printf("TWO %d, %d, %d, %d\n", v0,v1, v0/R, v1/R);
                // same column in s
                if(v1 / R == v0 / R && v1 /R == s)
                {
                    if(solvedInS.find(v0%R) == solvedInS.end() && solvedInS.find(v1%R) != solvedInS.end())
                    {
                        complement = modulo(v1,R);
//                                                printf("complement solve %d\n", modulo(v0,R));
                        return modulo(v0, R);
                    }
                    else if(solvedInS.find(v1%R) == solvedInS.end() && solvedInS.find(v0%R) != solvedInS.end())
                    {
                        complement = modulo(v0,R);
//                                                printf("complement solve %d\n", modulo(v1,R));
                        return modulo(v1, R);
                    }
                }
                // same row and not in s
                else if(v0 / R == r && v1 / R == t && v0 % R == v1 % R && solvedInS.find(modulo(*(missings.begin()),R)) == solvedInS.end())
                {
//                                        printf("seems horizontal solving %d, %d, solvable %d\n", v0,v1, v0 % R);
                    minusP.push_back(v0%R);
                    return v0 % R;
                }
                else
                {
//                                        printf("Unknown TWO missings %d %d\n",v0,v1);
//                                        getchar();
                }
            }
            else if(missings.size() == 4)
            {
                complement = -1;
                crosses.clear();
                for(int jj = 0;jj<ld;jj++)
                    crosses.push_back(make_pair(modulo(i+jj*v,K), modulo(j+jj*v,K)));
                set<int>::iterator it=missings.begin();
                int v0 = *it++;
                int v1 = *it++;
                int v2 = *it++;
                int v3 = *it;
//                                printf("FOUR missings %d,%d,%d,%d\n", v0,v1,v2,v3);
                if(v0/R == v1/R && v2/R == v3/R && v0%R==v2%R && v1%R == v3%R && v0/R == r && v2/R == t)
                {
//                                        printf("got column %d, %d, row %d,%d\n", v0/R, v2/R, v0%R, v1%R);
                    int vv0 = v0%R;
                    int vv1 = v1%R;
                    minusP.push_back(vv0);
                    minusP.push_back(vv1);
                    if(solvedInS.find(vv0) == solvedInS.end() && solvedInS.find(vv1) != solvedInS.end())
                    {
                        complement = (modulo(vv1,R));
//                                                printf("complement solve %d, need %d\n", modulo(vv0,R), modulo(vv1,R));
                        return modulo(vv0, R);
                    }
                    else if(solvedInS.find(vv1) == solvedInS.end() && solvedInS.find(vv0) != solvedInS.end())
                    {
                        complement = (modulo(vv0,R));
//                                                printf("complement solve %d, need %d\n", modulo(vv1,R), modulo(vv0,R));
                        return modulo(vv1, R);
                    }
                }
            }
            //            getchar();
        }
        missings.clear();
    }
    return -1;
}
