﻿#ifndef ZQFS_H
#define ZQFS_H
#include <stdlib.h>
#include <string.h>
#include <map>
#include <set>
extern "C"{
#include "../../Jerasure-1.2A/jerasure.h"
#include "../../Jerasure-1.2A/cauchy.h"
#include "../../Jerasure-1.2A/reed_sol.h"
}
#include <cassert>
#include <vector>
#include "../zcode.h"
#include "zschedule.h"
#include "../../utils.h"
using namespace std;

class ZQFS : public ZCode
{
public:
    ZQFS(int tK, int packetsize);
    ~ZQFS();
    void encode_single_chunk(char* data, int len, char**& parities);
    void set_erasure(vector<int> arr);
    void decode_single_chunk(char** &data, char** &parities);
    int K;
private:
    char** datp;
    int* erasures;
    int* enc_matrix;
};

#endif // ZQFS_H
