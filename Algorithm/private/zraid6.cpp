#include "zraid6.h"
#include <sys/time.h>
#include <x86intrin.h>
#include "../../utils.h"


static inline uint8_t mask_slow(uint8_t v)
{
    if (v < 128)
        return 0x00;
    else
        return 0xFF;
}

static inline uint8_t mul2_slow(uint8_t v)
{
    uint8_t vv = v + v;
    vv ^= mask_slow(v) & 0x1D;
    //    printf("mul2 %d, %d\n",v,vv);
    return vv;
}

//void printSet(set<int> &s, char* p)
//{
//    printf("%s: ", p);
//    for(set<int>::iterator it=s.begin(); it != s.end(); it++)
//        printf(" %d", *it);
//    printf("\n");
//}
//void printArray(vector<int> a, char* p)
//{
//    printf("%s: ", p);
//    for(int i = 0;i<a.size();i++)
//        printf("%d ", a[i]);
//    printf("\n");
//}

ZRaid6::ZRaid6(int tK, int m_packetsize): ZCode(tK,2,1,m_packetsize)
{
    K = tK;

    enc_matrix = (int*)malloc(M*K*sizeof(int));
    enc_matrix[0] = 1;
    enc_matrix[K] = 1;
    for(int j = 1;j<K;j++)
    {
        enc_matrix[j] = enc_matrix[j-1];
        enc_matrix[K+j] = mul2_slow(enc_matrix[K+j-1]);
    }

    if(packetsize == 0)
        packetsize = find_packetsize(K+2);
    printf("ZRaid6 K = %d, packetsize = %d\n", K, packetsize);

    blocksize = K*packetsize;
    datp = malloc2d(K, sizeof(char*));
    erasures = (int*) malloc(sizeof(int) * (K+M));
}

ZRaid6::~ZRaid6()
{
    free2d(datp,K);
    free(erasures);
}

void ZRaid6::encode_single_chunk(char* data, int len, char**& parities)
{
    assert(data != NULL);
    assert(parities != NULL);
    assert(len ==  packetsize * K);
    for(int i = 0;i<K;i++)
    {
        datp[i] = data + i*packetsize;
    }
    //    printf("-=-pin\n");
//    jerasure_matrix_encode(K,M,8,enc_matrix,datp,parities, packetsize);

    int loops = len/K;
    char* p,*q,*r,i,j;
    p = parities[0];
    q = parities[1];

    memcpy(p,data+(K-1)*packetsize,packetsize);
    memcpy(q,data+(K-1)*packetsize,packetsize);
    for (j = K-2; j >= 0; j--) {
        galois_region_xor(p,data+j*packetsize,p,packetsize);
        region_mul2(q,packetsize);
        galois_region_xor(q,data+j*packetsize,q,packetsize);
    }

}

void ZRaid6::set_erasure(vector<int> arr)
{
    int i;
    for(i = 0;i<arr.size();i++)
    {
        erasures[i] = arr[i];
    }
    erasures[i] = -1;
}


void ZRaid6::decode_single_chunk(char **&data, char **&parities)
{
    //    printf("decode_single chunk\n");
    jerasure_matrix_decode(K, M, 8, enc_matrix, 1, erasures, data, parities, packetsize);
}
