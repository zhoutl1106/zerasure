/*
main.cpp
Tianli Zhou

Fast Erasure Coding for Data Storage: A Comprehensive Study of the Acceleration Techniques

Revision 1.0
Mar 20, 2019

Tianli Zhou
Department of Electrical & Computer Engineering
Texas A&M University
College Station, TX, 77843
zhoutianli01@tamu.edu

Copyright (c) 2019, Tianli Zhou
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions
are met:

- Redistributions of source code must retain the above copyright
  notice, this list of conditions and the following disclaimer.

- Redistributions in binary form must reproduce the above copyright
  notice, this list of conditions and the following disclaimer in
  the documentation and/or other materials provided with the
  distribution.

- Neither the name of the Texas A&M University nor the names of its
  contributors may be used to endorse or promote products derived
  from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
"AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS
OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY
WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
POSSIBILITY OF SUCH DAMAGE.
*/
#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <string.h>
#include "Example/zexample.h"
#include "Algorithm/zgrouping.h"
#include <vector>
#include "Search/zrandomarray.h"
#include "Algorithm/private/zevenodd.h"
#include "Algorithm/private/zstar.h"
#include <iostream>
#include <algorithm>
using namespace std;

void usage()
{
    printf("Usage:\n");
    printf("\t./zerasure test_cost_weight [size] [loops]\n");
    printf("\t\t- Test the weight of memcpy and XOR in the schedule, \n\t\tusing data size [size] and run [loops] times\n\n");

    printf("\t... | ./zerasure single cost_func[0..2] strategy[0..7]\n");
    printf("\t\t- Read K,M,W,X,Y from stdin, generate schedule for given X,Y array\n");
    printf("\t\tusing cost function [cost_func] and strategy [strategy]\n\n");

    printf("\t./zerasure sa K M W S acc_rate cost_func[0..2] strategy[0..7]\n");
    printf("\t\t- Perform simulated annealing optimization, stop if minimum\n");
    printf("\t\thaven't change for S consecutive iterations. Initial rate\n");
    printf("\t\tof accept worse successor is given as acc_rate\n");
    printf("\t\tusing cost function [cost_func] and strategy [strategy]\n\n");

    printf("\t./zerasure ge K M W S init_population select_rate crossover_rate\n");
    printf("\t           tmutation_rate max_population cost_func[0..2] strategy[0..7]\n");
    printf("\t\t- Perform genetic optimization using given parameters, stop if minimum\n");
    printf("\t\thaven't change for S consecutive iterations. Initial rate\n");
    printf("\t\tusing cost function [cost_func] and strategy [strategy]\n\n");

    printf("\t... | ./zerasure code packetsize strategy[0..7]\n");
    printf("\t\t- Read K,M,W,X,Y from stdin, generate schedule for given\n");
    printf("\t\tusing cost function [cost_func] and strategy [strategy]\n");
}

//void print1d(char* p, int width, int height, char* txt)
//{
//    printf("%s:\n",txt);
//    for(int i = 0;i<width;i++)
//    {
//        for(int j = 0;j<height;j++)
//            printf("%4d ", p[i*width+j]);
//        printf("\n");
//    }
//}

//void print2d(char** p, int width, int height, char* txt)
//{
//    printf("%s:\n",txt);
//    for(int i = 0;i<width;i++)
//    {
//        for(int j = 0;j<height;j++)
//            printf("%4d ", p[j][i]);
//        printf("\n");
//    }
//}

static inline void print(char* p, char* txt)
{
    printf("%s\n", txt);
    for(int i = 0;i<64;i++)
    {
        printf("%4u ", p[i] & 0xff);
        if((i+1) % 16 == 0)
            printf("\n");
    }
    printf("\n");
}
static inline uint8_t mask_slow(uint8_t v)
{
    if (v < 128)
        return 0x00;
    else
        return 0xFF;
}

static inline uint8_t mul2_slow(uint8_t v)
{
    uint8_t vv = v + v;
    vv ^= mask_slow(v) & 0x1D;
    //    printf("mul2 %d, %d\n",v,vv);
    return vv;
}

static inline void region_mul2_slow(char* p, int len)
{
    int loops = len;
    for(int i = 0;i<loops;i++)
    {
        p[i] = mul2_slow(p[i]);
    }
}

int main(int argc, char *argv[])
{
    if(argc < 2)
    {
        usage();
        exit(-1);
    }

#ifdef VEC128
    printf("Using 128-bit vectorization\n");
#elif VEC256
    printf("Using 256-bit vectorization\n");
#elif VEC512
    printf("Using 512-bit vectorization\n");
#else
    printf("Unknown Vectorization\n");
#endif
    printf("\n\n ********* Start zerasure *********\n\n");

    if(strcmp(argv[1], "test_cost_weight") == 0)
        ZExample::test_cost_weight(argc,argv);

    else if(strcmp(argv[1], "single") == 0)
        ZExample::single(argc,argv);

    else if(strcmp(argv[1], "sa") == 0)
        ZExample::sa(argc,argv);

    else if(strcmp(argv[1], "ge") == 0)
        ZExample::ge(argc,argv);

    else if(strcmp(argv[1], "code") == 0)
        ZExample::code(argc,argv);

    else if(strcmp(argv[1], "array") == 0)
        ZExample::array(argc,argv);

    else
    {
        /************************ DEBUG ******************/

        int K = atoi(argv[2]), M = 3;
        ZStar *zc = new ZStar(K,1);
        int blocksize = zc->blocksize;
        int size = blocksize;
        char* data = (char*) aligned_alloc(64,zc->blocksize);
        for(int i = 0;i<zc->blocksize;i++)
            data[i] = i;

        char** par = malloc2d(M,zc->blocksize/K);
        zc->encode_single_chunk(data,zc->blocksize,par);

        char** adat = malloc2d(K,zc->blocksize/K);
        char** apar = malloc2d(M,zc->blocksize/K);
        char** parities = malloc2d(M, sizeof(char*));
        char** tdata = malloc2d(K, sizeof(char*));
        int pattern = atoi(argv[3]);

        while(1)
        {
            // gen lost pattern
            int n_lost = 0;
            vector<int> lost;
            while(1)
            {
                n_lost = 0;
                lost.clear();
                for(int i = 0;i<(K+M);i++)
                {
                    if((pattern & (1<<i)) != 0)
                    {
                        n_lost ++;
                        lost.push_back(i);
                    }
                }
                if(n_lost == 3) break;
                pattern ++;
                if(pattern > (1<<(K+M)))
                {
                    printf("!!! all set\n");
                    exit(0);
                }
            }



            printf("\n------\npattern %lld, n_lost %d\n",pattern, n_lost);
            printf("Lost : ");
            int cnt_par = 0;
            for(int i = 0;i<M;i++)
            {
                if(lost[i] < K)
                    printf(" data %d,", lost[i]);
                else
                {
                    printf(" parity %d,", lost[i]-K);
                    cnt_par ++;
                }
            }
            printf("\n");
            if(cnt_par == M)
            {
                printf("All parities, skip\n");
                //                continue;
            }
            sort(lost.begin(), lost.end());

            // copy of dat and par
            for(int j = 0;j<K;j++)
            {
                //            printf("copy dat off %d, size %d to adat[%d]\n", i*blocksize+j*blocksize/K, blocksize/K, j);
                memcpy(adat[j],data +j*blocksize/K,blocksize/K);
            }

            for(int j = 0;j<M;j++)
            {
                memcpy(apar[j], par[j], blocksize/K);
            }
            // set parity pointers
            for(int i = 0;i<M;i++)
                parities[i] = apar[i];

            // clear erased data
            for(int i = 0;i<M;i++)
            {
                if(lost[i] < K)
                {
                    //                            printf("clear adat[%d]\n",lost[i]);
                    memset(adat[lost[i]],0,size/K);
                }
                else
                {
                    //                            printf("clear par[%d]\n",lost[i]-K);
                    memset(apar[lost[i]-K],0,size/K);
                }
            }

            // Decode
            zc->set_erasure(lost);

            for(int i = 0;i<K;i++)
                tdata[i] = adat[i];
            zc->decode_single_chunk(tdata,parities);
            for(int j = 0;j<K;j++)
            {
                if(memcmp(tdata[j], data + j*blocksize/K, blocksize/K) != 0)
                {
                    printf("tdata[%d] diff, pattern = %d\n", j, pattern);
                    exit(0);
                }
            }

            printf("\n------\npattern %lld, n_lost %d\n",pattern, n_lost);
            pattern ++;
        }
        /**************************************************/
    }
    printf("\n\n *********  End zerasure **********\n\n");
    return 0;
}
