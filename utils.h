/*
utils.h
Tianli Zhou

Fast Erasure Coding for Data Storage: A Comprehensive Study of the Acceleration Techniques

Revision 1.0
Mar 20, 2019

Tianli Zhou
Department of Electrical & Computer Engineering
Texas A&M University
College Station, TX, 77843
zhoutianli01@tamu.edu

Copyright (c) 2019, Tianli Zhou
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions
are met:

- Redistributions of source code must retain the above copyright
  notice, this list of conditions and the following disclaimer.

- Redistributions in binary form must reproduce the above copyright
  notice, this list of conditions and the following disclaimer in
  the documentation and/or other materials provided with the
  distribution.

- Neither the name of the Texas A&M University nor the names of its
  contributors may be used to endorse or promote products derived
  from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
"AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS
OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY
WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
POSSIBILITY OF SUCH DAMAGE.
*/
#ifndef UTILS_H
#define UTILS_H

#include <x86intrin.h>
#include <vector>
#include <stdlib.h>
#include <string.h>
#include <cstdint>
#include <stdio.h>

using namespace std;

typedef uint8_t v16 __attribute__ ((vector_size (16)));
typedef uint8_t v32 __attribute__ ((vector_size (32)));
typedef uint8_t v64 __attribute__ ((vector_size (64)));

#define VEC16(x) ((v16){x,x,x,x, x,x,x,x, x,x,x,x, x,x,x,x})
#define VEC32(x) ((v32){x,x,x,x, x,x,x,x, x,x,x,x, x,x,x,x, x,x,x,x, x,x,x,x, x,x,x,x, x,x,x,x})
#define VEC64(x) ((v64){x,x,x,x, x,x,x,x, x,x,x,x, x,x,x,x, x,x,x,x, x,x,x,x, x,x,x,x, x,x,x,x, x,x,x,x, x,x,x,x, x,x,x,x, x,x,x,x, x,x,x,x, x,x,x,x, x,x,x,x, x,x,x,x})

long long diff_us(struct timeval start, struct timeval end);
long long diff_ms(struct timeval start, struct timeval end);

void fast_xor(char* r1, char* r2, char* r3, int size);
void slow_xor(char* r1, char* r2, char* r3, int size);

void freeVectorPointer(vector<int*> &p);
void free2dSchedule5(int **p);

char** malloc2d(int row, int col);
void free2d(char** &p, int len);

int find_packetsize(int factor);


#ifdef VEC128
static inline v16
mask(v16 v)
{
    return (v16)_mm_cmplt_epi8((__m128i)v, (__m128i)VEC16(0));
}

static inline v16
mul2(v16 v)
{
    v16 vv;

    vv = v + v;
    vv ^= mask(v) & VEC16(0x1d);
    return vv;
}

static inline void region_mul2(char* p, int len)
{
    int loops = len/16;
    for(int i = 0;i<loops;i++)
    {
        v16* a1 = (v16*)(p+i*16);
        *a1 = mul2(*a1);
    }
}

#elif VEC256
static inline v32
mask(v32 v)
{
    return (v32)_mm256_cmpgt_epi8((__m256i)VEC32(0),(__m256i)v);
}

static inline v32
mul2(v32 v)
{
    v32 vv;

    vv = v + v;
    vv ^= mask(v) & VEC32(0x1d);
    return vv;
}

static inline void region_mul2(char* p, int len)
{
    int loops = len/32;
    for(int i = 0;i<loops;i++)
    {
        v32* a1 = (v32*)(p+i*32);
        *a1 = mul2(*a1);
    }
}
#elif VEC512

static inline __mmask64
mask(__m512i v)
{
    return _mm512_cmplt_epi8_mask((__m512i)v,_mm512_set1_epi8(0));
}

static inline __m512i
mul2(__m512i v)
{
    __m512i vv;

    vv = _mm512_add_epi8(v,v);
    vv ^= _mm512_mask_set1_epi8(_mm512_set1_epi8(0),mask(v),0x1d);
    return vv;
}

static inline void region_mul2(char* p, int len)
{
    int loops = len/64;
    for(int i = 0;i<loops;i++)
    {
        __m512i* a1 = (__m512i*)(p+i*64);
        *a1 = mul2(*a1);
    }
}
#else

static inline uint8_t mask(uint8_t v)
{
    if (v < 128)
        return 0x00;
    else
        return 0xFF;
}

static inline uint8_t mul2(uint8_t v)
{
    uint8_t vv = v + v;
    vv ^= mask(v) & 0x1D;
    //    printf("mul2 %d, %d\n",v,vv);
    return vv;
}

static inline void region_mul2(char* p, int len)
{
    int loops = len;
    for(int i = 0;i<loops;i++)
    {
        p[i] = mul2(p[i]);
    }
}

#endif


#endif // UTILS_H
