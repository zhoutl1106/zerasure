OBJS=galois.o \
	reed_sol.o \
	jerasure.o \
	cauchy.o \
	main.o \
	zelement.o \
	zgenetic.o \
	utils.o \
	zexample.o \
	zrandomarray.o \
	zoxc.o \
	zsimulatedannealing.o \
	zcauchy.o \
	zcode.o \
	zgrouping.o \
	zraid6.o \
	zevenodd.o \
	zrdp.o \
	zqfs.o \
	zstar.o \
	zschedule.o

CC=gcc
CXX=g++
LEMON_DIR=../usr/local/include
GF_DIR=../gf-complete
CFLAGS=-O3 -I$(GF_DIR)/include
CXXFLAGS=-O3 -D$(OPT)
OPT=abc

ifeq ($(VEC), VEC128)
	CFLAGS=-msse4 -O3 -D$(VEC) -I$(GF_DIR)/include
        CXXFLAGS=-msse4 -O3 -D$(VEC) -D$(OPT) -I$(LEMON_DIR)
endif
ifeq ($(VEC), VEC256)
	CFLAGS=-mavx2 -O3 -D$(VEC) -I$(GF_DIR)/include
        CXXFLAGS=-mavx2 -O3 -D$(VEC) -D$(OPT) -I$(LEMON_DIR)
endif
ifeq ($(VEC), VEC512)
	CFLAGS=-march=skylake-avx512 -O3 -D$(VEC) -I$(GF_DIR)/include
        CXXFLAGS=-march=skylake-avx512 -O3 -D$(VEC) -D$(OPT) -I$(LEMON_DIR) -L../lib64 -L../lib
endif

zerasure: $(OBJS) makefile
	$(CXX) -o zerasure $(OBJS) -L/home/zhou/lib/ -lgf_complete

#galois.o: Jerasure-1.2A/galois.c Jerasure-1.2A/galois.h makefile
#	$(CC) -c Jerasure-1.2A/galois.c $(CFLAGS)

#jerasure.o: Jerasure-1.2A/jerasure.c Jerasure-1.2A/jerasure.h makefile
#	$(CC) -c Jerasure-1.2A/jerasure.c $(CFLAGS)

#reed_sol.o: Jerasure-1.2A/reed_sol.c Jerasure-1.2A/reed_sol.h makefile
#	$(CC) -c Jerasure-1.2A/reed_sol.c $(CFLAGS)

#cauchy.o: Jerasure-1.2A/cauchy.c Jerasure-1.2A/cauchy.h makefile
#	$(CC) -c Jerasure-1.2A/cauchy.c $(CFLAGS)

galois.o: j2.0/galois.c j2.0/galois.h makefile
	$(CC) -c j2.0/galois.c $(CFLAGS)

jerasure.o: j2.0/jerasure.c j2.0/jerasure.h makefile
	$(CC) -c j2.0/jerasure.c $(CFLAGS)

reed_sol.o: j2.0/reed_sol.c j2.0/reed_sol.h makefile
	$(CC) -c j2.0/reed_sol.c $(CFLAGS)

cauchy.o: j2.0/cauchy.c j2.0/cauchy.h makefile
	$(CC) -c j2.0/cauchy.c $(CFLAGS)

main.o : main.cpp makefile
	$(CXX) -c main.cpp $(CXXFLAGS)
zelement.o : Search/zelement.cpp Search/zelement.h makefile
	$(CXX) -c Search/zelement.cpp $(CXXFLAGS)
zgenetic.o : Search/zgenetic.cpp Search/zgenetic.h makefile
	$(CXX) -c Search/zgenetic.cpp $(CXXFLAGS)
utils.o : utils.cpp utils.h makefile
	$(CXX) -c utils.cpp $(CXXFLAGS)
zexample.o : Example/zexample.cpp Example/zexample.h makefile
	$(CXX) -c Example/zexample.cpp $(CXXFLAGS)
zrandomarray.o : Search/zrandomarray.cpp Search/zrandomarray.h makefile
	$(CXX) -c Search/zrandomarray.cpp $(CXXFLAGS)
zoxc.o : Algorithm/zoxc.cpp Algorithm/zoxc.h makefile
	$(CXX) -c Algorithm/zoxc.cpp $(CXXFLAGS)
zsimulatedannealing.o : Search/zsimulatedannealing.cpp Search/zsimulatedannealing.h makefile
	$(CXX) -c Search/zsimulatedannealing.cpp $(CXXFLAG)
zcauchy.o : Algorithm/zcauchy.cpp Algorithm/zcauchy.h makefile
	$(CXX) -c Algorithm/zcauchy.cpp $(CXXFLAGS)
zcode.o : Algorithm/zcode.cpp Algorithm/zcode.h makefile
	$(CXX) -c Algorithm/zcode.cpp $(CXXFLAGS)
zgrouping.o : Algorithm/zgrouping.cpp Algorithm/zgrouping.h makefile
	$(CXX) -c Algorithm/zgrouping.cpp $(CXXFLAGS)
zraid6.o : Algorithm/private/zraid6.cpp Algorithm/private/zraid6.h makefile
	$(CXX) -c Algorithm/private/zraid6.cpp $(CXXFLAGS)
zevenodd.o : Algorithm/private/zevenodd.cpp Algorithm/private/zevenodd.h makefile
	$(CXX) -c Algorithm/private/zevenodd.cpp $(CXXFLAGS)
zrdp.o : Algorithm/private/zrdp.cpp Algorithm/private/zrdp.h makefile
	$(CXX) -c Algorithm/private/zrdp.cpp $(CXXFLAGS)
zstar.o : Algorithm/private/zstar.cpp Algorithm/private/zstar.h makefile
	$(CXX) -c Algorithm/private/zstar.cpp $(CXXFLAGS)
zqfs.o : Algorithm/private/zqfs.cpp Algorithm/private/zqfs.h makefile
	$(CXX) -c Algorithm/private/zqfs.cpp $(CXXFLAGS)
zschedule.o : Algorithm/private/zschedule.cpp Algorithm/private/zschedule.h makefile
	$(CXX) -c Algorithm/private/zschedule.cpp $(CXXFLAGS)

.PHONY: clean
clean:
	rm zerasure $(OBJS)
