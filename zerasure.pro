TEMPLATE = app
CONFIG += console
CONFIG -= app_bundle
CONFIG -= qt
VEC=VEC256
LEMON_DIR=../usr/local/include
QMAKE_CXXFLAGS += -lemon -O3 -msse4 -mavx2 -D$${VEC} -I$${LEMON_DIR}
QMAKE_CFLAGS += -lemon -O3 -msse4 -mavx2 -D$${VEC}
QMAKE_CC = gcc
QMAKE_CXX = g++

SOURCES += \
    Jerasure-1.2A/galois.c \
    Jerasure-1.2A/jerasure.c \
    Jerasure-1.2A/reed_sol.c \
    Jerasure-1.2A/cauchy.c \ 
    main.cpp \
    utils.cpp \
    Example/zexample.cpp \
    Search/zelement.cpp \
    Search/zgenetic.cpp \
    Search/zrandomarray.cpp \
    Algorithm/zoxc.cpp \
    Search/zsimulatedannealing.cpp \
    Algorithm/zcauchy.cpp \
    Algorithm/zgrouping.cpp \
    Algorithm/zcode.cpp \
    Algorithm/private/zraid6.cpp \
    Algorithm/private/zschedule.cpp \
    Algorithm/private/zevenodd.cpp \
    Algorithm/private/zqfs.cpp \
    Algorithm/private/zrdp.cpp \
    Algorithm/private/zstar.cpp \
    Algorithm/private/zevenodd.cpp \
    Algorithm/private/zqfs.cpp \
    Algorithm/private/zraid6.cpp \
    Algorithm/private/zrdp.cpp \
    Algorithm/private/zschedule.cpp \
    Algorithm/private/zstar.cpp \
    Algorithm/zcauchy.cpp \
    Algorithm/zcode.cpp \
    Algorithm/zgrouping.cpp \
    Algorithm/zoxc.cpp \
    Example/zexample.cpp \
    isal/main.cpp \
    isal/measurements.cpp \
    isal/options.cpp \
    isal/prealloc.cpp \
    isal/size.cpp \
    isal/utils.cpp \
    Search/zelement.cpp \
    Search/zgenetic.cpp \
    Search/zrandomarray.cpp \
    Search/zsimulatedannealing.cpp \
    main.cpp \
    utils.cpp \
    j2.0/cauchy.c \
    j2.0/galois.c \
    j2.0/jerasure.c \
    j2.0/reed_sol.c \
    Jerasure-1.2A/cauchy.c \
    Jerasure-1.2A/galois.c \
    Jerasure-1.2A/jerasure.c \
    Jerasure-1.2A/reed_sol.c

HEADERS += \
    Jerasure-1.2A/galois.h \
    Jerasure-1.2A/jerasure.h \
    Jerasure-1.2A/reed_sol.h \
    Jerasure-1.2A/cauchy.h \
    utils.h \
    Example/zexample.h \
    Search/zelement.h \
    Search/zgenetic.h \
    Search/zrandomarray.h \
    Algorithm/zoxc.h \
    Search/zsimulatedannealing.h \
    Algorithm/zcauchy.h \
    Algorithm/zgrouping.h \
    Algorithm/zcode.h \
    Algorithm/private/zraid6.h \
    Algorithm/private/zschedule.h \
    Algorithm/private/zevenodd.h \
    Algorithm/private/zqfs.h \
    Algorithm/private/zrdp.h \
    Algorithm/private/zstar.h \
    Algorithm/private/zevenodd.h \
    Algorithm/private/zqfs.h \
    Algorithm/private/zraid6.h \
    Algorithm/private/zrdp.h \
    Algorithm/private/zschedule.h \
    Algorithm/private/zstar.h \
    Algorithm/zcauchy.h \
    Algorithm/zcode.h \
    Algorithm/zgrouping.h \
    Algorithm/zoxc.h \
    Example/zexample.h \
    isal/measurements.hpp \
    isal/options.h \
    isal/prealloc.h \
    isal/random_number_generator.h \
    isal/size.h \
    isal/utils.hpp \
    j2.0/cauchy.h \
    j2.0/galois.h \
    j2.0/jerasure.h \
    j2.0/reed_sol.h \
    Jerasure-1.2A/cauchy.h \
    Jerasure-1.2A/galois.h \
    Jerasure-1.2A/jerasure.h \
    Jerasure-1.2A/reed_sol.h \
    Search/zelement.h \
    Search/zgenetic.h \
    Search/zrandomarray.h \
    Search/zsimulatedannealing.h \
    utils.h


DISTFILES += \
    README.md \
    mfile \
    makefile
