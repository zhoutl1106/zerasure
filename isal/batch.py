import os
import sys

m5 = [2,3]
m6 = [2,3,4]
m7 = [2,3,4]
m8 = [2,3,4]
m9 = [2,3,4]
m10 = [2,3,4,5,6]

for i in m5:
	sys.stderr.write("5,{}".format(i))
	sys.stderr.write('\n')
	os.system('./ex2 --buffer-count {} --lost-buffers {}'.format(5+i,i))
	sys.stdout.write('\n')
	sys.stdout.flush();

for i in m6:
	sys.stderr.write("6,{}".format(i))
	sys.stderr.write('\n')
	os.system('./ex2 --buffer-count {} --lost-buffers {}'.format(6+i,i))
	sys.stdout.write('\n')
	sys.stdout.flush();

for i in m7:
	sys.stderr.write("7,{}".format(i))
	sys.stderr.write('\n')
	os.system('./ex2 --buffer-count {} --lost-buffers {}'.format(7+i,i))
	sys.stdout.write('\n')
	sys.stdout.flush();

for i in m8:
	sys.stderr.write("8,{}".format(i))
	sys.stderr.write('\n')
	os.system('./ex2 --buffer-count {} --lost-buffers {}'.format(8+i,i))
	sys.stdout.write('\n')
	sys.stdout.flush();

for i in m9:
	sys.stderr.write("9,{}".format(i))
	sys.stderr.write('\n')
	os.system('./ex2 --buffer-count {} --lost-buffers {}'.format(9+i,i))
	sys.stdout.write('\n')
	sys.stdout.flush();

for i in m10:
	sys.stderr.write("10,{}".format(i))
	sys.stderr.write('\n')
	os.system('./ex2 --buffer-count {} --lost-buffers {}'.format(10+i,i))
	sys.stdout.write('\n')
	sys.stdout.flush();
