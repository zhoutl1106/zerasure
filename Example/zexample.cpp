/*
zexample.cpp
Tianli Zhou

Fast Erasure Coding for Data Storage: A Comprehensive Study of the Acceleration Techniques

Revision 1.0
Mar 20, 2019

Tianli Zhou
Department of Electrical & Computer Engineering
Texas A&M University
College Station, TX, 77843
zhoutianli01@tamu.edu

Copyright (c) 2019, Tianli Zhou
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions
are met:

- Redistributions of source code must retain the above copyright
  notice, this list of conditions and the following disclaimer.

- Redistributions in binary form must reproduce the above copyright
  notice, this list of conditions and the following disclaimer in
  the documentation and/or other materials provided with the
  distribution.

- Neither the name of the Texas A&M University nor the names of its
  contributors may be used to endorse or promote products derived
  from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
"AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS
OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY
WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
POSSIBILITY OF SUCH DAMAGE.
*/
#include "zexample.h"
#include "../Search/zelement.h"
#include <stdio.h>
#include <stdlib.h>
#include <iostream>
#include <vector>
#include <algorithm>
#include "../Search/zsimulatedannealing.h"
#include "../Search/zgenetic.h"
#include "../Algorithm/zcauchy.h"
#include "../Algorithm/zgrouping.h"

#include "../Algorithm/private/zraid6.h"
#include "../Algorithm/private/zevenodd.h"
#include "../Algorithm/private/zrdp.h"
#include "../Algorithm/private/zstar.h"
#include "../Algorithm/private/zqfs.h"

using namespace std;

ZExample::ZExample()
{

}

void ZExample::test_cost_weight(int argc, char *argv[])
{
    ZElement::init(0,0,0,0,0);
    ZElement* e = new ZElement();
    if(argc == 2)
        e->test_cost_weight();
    else if(argc == 3)
        e->test_cost_weight(atoi(argv[2]));
    else if(argc == 4)
        e->test_cost_weight(atoi(argv[2]),atoi(argv[3]));
    else
        printf("usage: size loops\n");
    delete e;
}

void ZExample::single(int argc, char *argv[])
{
    if(argc == 4)
    {
        int K,M,W,costf,stra;
        cin >> K >> M >> W;
        costf = atoi(argv[2]);
        stra = atoi(argv[3]);
        vector<int> arr(K+M,0);
        for(int i = 0;i<K+M;i++)
            cin >> arr[i];

        printf("cost_func = %d, strategy = %d, K = %d, M = %d, W = %d, arr = ",costf,stra,K,M,W);
        for(int i = 0;i<K+M;i++)
            printf("%d ",arr[i]);
        printf("\n");
        ZElement::init(K,M,W,costf,stra);
        ZElement* e = new ZElement(arr.data());
        long long ret = e->value();
        printf("Cost for given input array : %d\n", ret);
        delete e;
    }
    else
        printf("usage: cost_func[0..2] strategy[0..7]\n");
}

void ZExample::sa(int argc, char *argv[])
{
    if(argc == 9)
    {
        int K,M,W,S,costf,stra;
        double acc_rate;
        K = atoi(argv[2]);
        M = atoi(argv[3]);
        W = atoi(argv[4]);
        S = atoi(argv[5]);
        acc_rate = atof(argv[6]);
        costf = atoi(argv[7]);
        stra = atoi(argv[8]);

        printf("cost_func = %d, strategy = %d\n", costf, stra);
        ZElement::init(K,M,W,costf,stra);
        ZSimulatedAnnealing sa(K,M,W,S,acc_rate);
        sa.run();
    }
    else
        printf("usage: K M W S acc_rate cost_func[0..2] strategy[0..7]\n");
}

void ZExample::ge(int argc, char *argv[])
{
    if(argc == 13)
    {
        int K,M,W,S,init_pop, max_pop,costf,stra;
        double select_rate, crossover_rate, mutation_rate;
        K = atoi(argv[2]);
        M = atoi(argv[3]);
        W = atoi(argv[4]);
        S = atoi(argv[5]);
        init_pop = atoi(argv[6]);
        select_rate = atof(argv[7]);
        crossover_rate = atof(argv[8]);
        mutation_rate = atof(argv[9]);
        max_pop = atoi(argv[10]);
        costf = atoi(argv[11]);
        stra = atoi(argv[12]);

        printf("cost_func = %d, strategy = %d\n", costf, stra);
        ZElement::init(K,M,W,costf,stra);
        ZGenetic ge(K,M,W,S,init_pop,select_rate,crossover_rate,mutation_rate,max_pop);
        ge.run();
    }
    else
        printf("usage: K M W S init_population select_rate crossover_rate tmutation_rate max_population cost_func[0..2] strategy[0..7]\n");
}

void ZExample::code(int argc, char *argv[])
{
    if(argc == 4)
    {
        int K,M,W,packetsize,stra;
        cin >> K >> M >> W;
        packetsize = atoi(argv[2]);
        stra = atoi(argv[3]);
        vector<int> arr(K+M,0);
        for(int i = 0;i<K+M;i++)
            cin >> arr[i];

        printf("packetsize = %d, strategy = %d, K = %d, M = %d, W = %d, arr = ",packetsize,stra,K,M,W);
        for(int i = 0;i<K+M;i++)
            printf("%d ",arr[i]);
        printf("\n");

        //    const int packetsize = 1;
        ZCode *zc;
        bool isSmart;
        bool isNormal;
        bool isWeightedGrouping;

        switch(stra)
        {
        case 0:
            // natual dumb
            isSmart = false;
            isNormal = false;
            zc = (ZCode*) new ZCauchy(K,M,W,arr,isSmart,isNormal,packetsize);
            break;
        case 1:
            // natual smart
            isSmart = true;
            isNormal = false;
            zc = (ZCode*) new ZCauchy(K,M,W,arr,isSmart,isNormal,packetsize);
            break;
        case 2:
            // natual grouping unweighed
            isNormal = false;
            isWeightedGrouping = false;
            zc = (ZCode*) new ZGrouping(K,M,W,arr,isNormal,isWeightedGrouping,packetsize);
            break;
        case 3:
            // natual grouping weighted
            isNormal = false;
            isWeightedGrouping = true;
            zc = (ZCode*) new ZGrouping(K,M,W,arr,isNormal,isWeightedGrouping,packetsize);
            break;
        case 4:
            // normal dumb
            isSmart = false;
            isNormal = true;
            zc = (ZCode*) new ZCauchy(K,M,W,arr,isSmart,isNormal,packetsize);
            break;
        case 5:
            // normal smart
            isSmart = true;
            isNormal = true;
            zc = (ZCode*) new ZCauchy(K,M,W,arr,isSmart,isNormal,packetsize);
            break;
        case 6:
            // normal unweighted
            isNormal = true;
            isWeightedGrouping = false;
            zc = (ZCode*) new ZGrouping(K,M,W,arr,isNormal,isWeightedGrouping,packetsize);
            break;
        case 7:
            // normal weighted
            isNormal = true;
            isWeightedGrouping = true;
            zc = (ZCode*) new ZGrouping(K,M,W,arr,isNormal,isWeightedGrouping,packetsize);
            break;
        default:
            printf("Unknown strategy, exiting...\n");
            exit(0);
        }
        zc->test_speed();
    }
    else
        printf("usage: packetsize strategy[0..7]\n");
}

void ZExample::array(int argc, char *argv[])
{
    if(argc == 5)
    {
        int K,packetsize;
        K = atoi(argv[3]);
        packetsize = atoi(argv[4]);
        ZCode *zc;
        if(strcmp(argv[2], "raid6") == 0)
            zc = new ZRaid6(K,packetsize);
        if(strcmp(argv[2], "evenodd") == 0)
            zc = new ZEvenOdd(K,packetsize);
        if(strcmp(argv[2], "rdp") == 0)
            zc = new ZRDP(K,packetsize);
        if(strcmp(argv[2], "star") == 0)
            zc = new ZStar(K,packetsize);
        if(strcmp(argv[2], "qfs") == 0)
            zc = new ZQFS(K,packetsize);

        zc->test_speed();
    }
    else
        printf("usage: array_code_name[raid6,] K packetsize \n");
}


void ZExample::test_array_speed(int code, int K)
{/*
    // gen original data
//    int size = 100*1024*1024;
    int size = blocksize * 2;
    // padded to multiple blocksize
    while(size%(blocksize) != 0)
        size ++;
    int loops = size/(blocksize);
    printf("size is %d Byte, %d loops\n",size,loops);
    char *dat = (char*)aligned_alloc(64,size);
    for(int i = 0;i<size;i++)
    {
        dat[i] = rand();
    }

    char** par = malloc2d(M, size/K);
    char** parities = (char**) aligned_alloc(64, sizeof(char*)*M);
    for(int i = 0;i<M;i++)
        parities[i] = par[i];
    // Encode
    struct timeval t0,t1;
    gettimeofday(&t0,NULL);
    for(int i = 0;i<loops;i++)
    {
        encode_single_chunk(dat+i*blocksize,blocksize,parities);
        for(int j = 0;j<M;j++)
        {
            parities[j] += blocksize/K;
        }
    }
    gettimeofday(&t1,NULL);
    long long diff = diff_us(t0,t1);
    printf(" !!! Encode cost %d us, init %d us, total %d us, size = %d, speed = %f MB/s\n", diff, init_time, diff+init_time, size, size * 1000.0 * 1000/ 1024.0/1024 / (diff));

    // gen erased data
    char** adat = malloc2d(K, size/K);  // copy of systemtic nodes
    char** apar = malloc2d(M, size/K);  // copy of parities

//    int dec_loops = 0;
    ZRandomArray zra(K+M);
    long long dec_time = 0;
    char** tdata = malloc2d(K, sizeof(char*));  // 2d data pointer
    // run 100 times different erasures
//    while(dec_loops < 100)
    {
        // gen lost pattern
        int* p = zra.next_random(K+M,K+M);
        vector<int> lost;
//        printf("%d Lost : ", dec_loops);
        int cnt_par = 0;
        for(int i = 0;i<M;i++)
        {
            if(p[i] < K)
                printf(" data %d,", p[i]);
            else
            {
                printf(" parity %d,", p[i]-K);
                cnt_par ++;
            }
            lost.push_back(p[i]);
        }
        printf("\n");
//        if(cnt_par == M)
//        {
//            printf("All parities, skip\n");
//            continue;
//        }
//        dec_loops ++;
        sort(lost.begin(), lost.end());

        // copy of dat and par
        for(int i = 0;i<loops;i++)
        {
            for(int j = 0;j<K;j++)
            {
                //            printf("copy dat off %d, size %d to adat[%d]\n", i*blocksize+j*blocksize/K, blocksize/K, j);
                memcpy(adat[j]+i*blocksize/K,dat + i*blocksize+j*blocksize/K,blocksize/K);
            }
        }
        for(int j = 0;j<M;j++)
        {
            memcpy(apar[j], par[j], size/K);
        }

        // set parity pointers
        for(int i = 0;i<M;i++)
            parities[i] = apar[i];

        // clear erased data
        for(int i = 0;i<M;i++)
        {
            if(lost[i] < K)
            {
                //            printf("clear adat[%d]\n",i);
                memset(adat[lost[i]],0,size/K);
            }
            else
            {
                //            printf("clear par[%d]\n",lost[i]-K);
                memset(apar[lost[i]-K],0,size/K);
            }
        }

        // Decode
        set_erasure(lost);

        for(int i = 0;i<K;i++)
            tdata[i] = adat[i];
        gettimeofday(&t0,NULL);
        for(int i = 0;i<loops;i++)
        {
            decode_single_chunk(tdata,parities);
            for(int j = 0;j<K;j++)
                tdata[j] += blocksize/K;
            for(int j = 0;j<M;j++)
                parities[j] += blocksize/K;
        }
        gettimeofday(&t1,NULL);
        dec_time += diff_us(t0,t1);

        // end one loop, reset pointers
        for(int i = 0;i<K;i++)
            tdata[i] = adat[i];
        for(int i = 0;i<M;i++)
            parities[i] = apar[i];

        // Check correctness
        bool isSame = true;
        for(int i = 0;i<loops;i++)
        {
            for(int j = 0;j<K;j++)
            {
                if(memcmp(tdata[j], dat + i*blocksize + j*blocksize/K, blocksize/K) != 0)
                {
                    isSame = false;
                    printf("loop %d, tdata[%d] diff\n", i,j);
//                    print1DArr(dat + i*blocksize + j*blocksize/K, blocksize/K,dat + i*blocksize + j*blocksize/K);
//                    print1DArr(tdata[j],blocksize/K,"tdata");
//                    for(int k = 0;k<blocksize/K;k++)
//                    {
//                        if(tdata[j][k] != *(dat + i*blocksize + j*blocksize/K + k))
//                        {
//                            printf("diff at %d\n",k);
//                            break;
//                        }
//                    }
                    break;
                }
            }
            if(!isSame) break;

            for(int j = 0;j<K;j++)
                tdata[j] += blocksize/K;
        }
        if(!isSame)
        {
            printf(" ??? Diff \n");
//            printf(" ??? Diff at loop %d\n", dec_loops);
//            exit(-1);
        }
    }
    diff = dec_time / 1;
    printf(" !!! Decode cost %d us, init %d us, total %d us, size = %d, speed = %f MB/s\n", diff, init_time, diff+init_time, size, size * 1000.0 * 1000/ 1024.0/1024 / (diff));

    free(adat);*/
}
